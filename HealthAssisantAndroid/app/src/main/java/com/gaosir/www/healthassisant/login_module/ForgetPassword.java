package com.gaosir.www.healthassisant.login_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVOSCloud;
import com.gaosir.www.healthassisant.R;

/**
 * @author Chanlone Gogh
 * @version 1.1.0 2016/8/9.
 */
public class ForgetPassword extends Activity implements View.OnClickListener {

    private static final String mTag = "ForgetP";
    private EditText mNumber;
    private EditText mRequest;
    private String mPhoneNumber;

    private Boolean mState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.forget_password);

        initView();
    }

    private void initView() {
        mNumber = (EditText) findViewById(R.id.et_user_number);
        mRequest = (EditText) findViewById(R.id.et_request);


        findViewById(R.id.btn_get_request_code).setClickable(true);
        findViewById(R.id.btn_get_request_code).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_request_code:
                mPhoneNumber = mNumber.getText().toString().trim();
                if (mPhoneNumber.length() != 11) {
                    Toast.makeText(ForgetPassword.this, getResources().getString(R.string.register_number_illegal), Toast.LENGTH_SHORT).show();
                    break;
                }
                //TODO 处理忘记密码，业务可能有问题
                try {
                    AVOSCloud.requestSMSCode(mPhoneNumber, getResources().getString(R.string.app_name), getResources().getString(R.string.forget_password), 5);
                } catch (AVException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_submit:
                if (mState) {
                    String mRequestCode;
                    mRequestCode = mRequest.getText().toString().trim();
                    try {
                        AVOSCloud.verifySMSCode(mRequestCode, mPhoneNumber);
                    } catch (AVException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.back:
                finish();
                break;
        }

    }
}
