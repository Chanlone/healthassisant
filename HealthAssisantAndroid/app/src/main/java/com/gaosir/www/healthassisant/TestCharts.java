package com.gaosir.www.healthassisant;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-06-18
 */
public class TestCharts extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.test_chart);
        LineChart chart = (LineChart) findViewById(R.id.chart);
        PieChart peiChart = (PieChart) findViewById(R.id.pie_chart);
        LineData mLineData = getLineData(10,30);
        showChart(chart,mLineData);
        PieData mPieData = getPieData(4,100);
        showPieChart(peiChart,mPieData);


//        chart.setDescriptionPosition(0.5f,0.5f);
    }
    private void showChart(LineChart lineChart, LineData lineData ){
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setAxisLineWidth(3f);
//        yAxis.setAxisLineColor(getResources().getColor(R.color.holo_red_dark));
        yAxis.setAxisMaxValue(40f);
        LimitLine ll = new LimitLine(28f,"警戒线");
        ll.setLineColor(getResources().getColor(R.color.holo_red_dark));
        ll.setLineWidth(4f);
        ll.setTextSize(14f);
        yAxis.addLimitLine(ll);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineWidth(3f);
//        xAxis.setAxisLineColor(Color.YELLOW);

        lineChart.setDrawBorders(false);
        lineChart.setDescription(getResources().getString(R.string.evaluation));
        lineChart.setNoDataTextDescription("You need to provide data for the chart.");
        lineChart.setBackgroundColor(getResources().getColor(R.color.normal_bg_color));
        lineChart.setDrawGridBackground(false);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setData(lineData);
        Legend mLegend = lineChart.getLegend();
        mLegend.setForm(Legend.LegendForm.LINE);
        lineChart.animateY(2000);
    }


    private LineData getLineData(int count , float range){
        ArrayList<String> xValues = new ArrayList<>();
        for (int i = 0;i<count;i++){
            xValues.add(""+i);
        }

        ArrayList<Entry> yValues = new ArrayList<>();
        for (int i = 0; i<count; i++){
            float value = (float) ((Math.random()*range)+3);
            yValues.add(new Entry(value,i));
        }

        LineDataSet lineDataSet = new LineDataSet(yValues,"Chart");

        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(lineDataSet);

        return new LineData(xValues,lineDataSets);
    }
    private void showPieChart(PieChart pieChart,PieData pieData){
        pieChart.setHoleColorTransparent(true);

        pieChart.setHoleRadius(60f);  //半径
        pieChart.setTransparentCircleRadius(30f); // 半透明圈
        pieChart.setDrawCenterText(true);  //饼状图中间可以添加文字
        pieChart.setCenterTextSize(18f);
        pieChart.setHoleRadius(25f);  //实心圆
        pieChart.setBackgroundColor(getResources().getColor(R.color.normal_bg_color));

        pieChart.setDescription("心率分布图");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setRotationAngle(90); // 初始旋转角度
        pieChart.setRotationEnabled(true); // 可以手动旋转

        pieChart.setUsePercentValues(true);  //显示成百分比
        pieChart.setCenterText("心率");  //饼状图中间的文字

        //设置数据
        pieChart.setData(pieData);

        Legend mLegend = pieChart.getLegend();  //设置比例图
        mLegend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);  //最右边显示
//      mLegend.setForm(LegendForm.LINE);  //设置比例图的形状，默认是方形
        mLegend.setXEntrySpace(7f);
        mLegend.setYEntrySpace(5f);

        pieChart.animateXY(1500, 1500);
    }

    private PieData getPieData(int count, float range){
        ArrayList<String> xValues = new ArrayList<>();
        xValues.add("心率60以下");
        xValues.add("心率60-75");
        xValues.add("心率75-90");
        xValues.add("心率90-110");

        ArrayList<Entry> yValues = new ArrayList<>();
        float qu1= 14;
        float qu2= 28;
        float qu3= 32;
        float qu4= 26;

        yValues.add(new Entry(qu1,0));
        yValues.add(new Entry(qu2,1));
        yValues.add(new Entry(qu3, 2));
        yValues.add(new Entry(qu4, 3));

        PieDataSet pieDataSet = new PieDataSet(yValues,"心率分布图");
        pieDataSet.setSliceSpace(0f);
        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(Color.rgb(135,200,250));
        colors.add(Color.rgb(255, 165, 80));
        colors.add(Color.rgb(240, 220, 130));
        colors.add(Color.rgb(180, 240, 60));

        pieDataSet.setColors(colors);
        pieDataSet.setValueTextColor(getResources().getColor(R.color.normal_bg_color));

        DisplayMetrics metrics = getResources().getDisplayMetrics();

        return new PieData(xValues, pieDataSet);
    }
}