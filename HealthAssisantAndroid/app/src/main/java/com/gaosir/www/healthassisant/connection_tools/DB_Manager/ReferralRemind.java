package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

public class ReferralRemind {
	public int _id;
	public String user_name = null;
	public String referral_title = null;
	public String start_date = null;
	public String remind_time = null;
	public String remind_place = null;
	public String remarks = null;
	public String alarmID = null;

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getReferral_title() {
		return referral_title;
	}

	public void setReferral_title(String referral_title) {
		this.referral_title = referral_title;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getRemind_time() {
		return remind_time;
	}

	public void setRemind_time(String remind_time) {
		this.remind_time = remind_time;
	}

	public String getRemind_place() {
		return remind_place;
	}

	public void setRemind_place(String remind_place) {
		this.remind_place = remind_place;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAlarmID() {
		return alarmID;
	}

	public void setAlarmID(String alarmID) {
		this.alarmID = alarmID;
	}

	public ReferralRemind() {
	}

	public ReferralRemind(String user_name, String referral_title,
			String start_date, String remind_time, String remind_place,
			String repeat_type, String repeat_times, String remarks,
			String alarmID) {
		this.user_name = user_name;
		this.referral_title = referral_title;
		this.start_date = start_date;
		this.remind_time = remind_time;
		this.remind_place = remind_place;
		this.remarks = remarks;
		this.alarmID = alarmID;
	}

}
