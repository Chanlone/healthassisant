package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class ReferralDBManager {

	private AlarmDBHelper helper;
	private static SQLiteDatabase db;

	public ReferralDBManager(Context context) {
		helper = new AlarmDBHelper(context);
		db = helper.getWritableDatabase();
	}

	public void add(Context context, ReferralRemind referralRemind) {
		db.beginTransaction();
		try {
			db.execSQL(
					"INSERT INTO referral_remind VALUES(?,?,?,?,?,?,?,?)",
					new Object[] { referralRemind._id,
							referralRemind.user_name,
							referralRemind.referral_title,
							referralRemind.start_date,
							referralRemind.remind_time,
							referralRemind.remind_place,
							referralRemind.remarks, referralRemind.alarmID });
			db.setTransactionSuccessful();
		} finally {
			Log.i("Gao", "insert complete");
			db.endTransaction();
		}

	}

	public int getMaxID() {
		int id = 0;
		Cursor cursor = queryTheCursor();
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToLast();
			id = cursor.getInt(cursor.getColumnIndex("_id"));
		}
		cursor.close();
		return id;
	}

	public void deleteData(Context context, String _idString) {
		db.delete("referral_remind", "_id = ?",
				new String[] { String.valueOf(_idString) });
	}
	
	public String alarmIDStrings(Context context, String alarmID) {
		String referralNames = "";
		Cursor cursor = querySameTime(alarmID);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				referralNames += cursor.getString(cursor
						.getColumnIndex("referral_title"));
			}
		}
		return referralNames;
	}

	public static Cursor querySameTime(String alarmID) {
		Cursor cursor = db
				.rawQuery(
						"SELECT * FROM referral_remind WHERE user_name = ? and alarmID = ?",
						new String[] {
								"user",
								alarmID });
		return cursor;
	}

	public void updateData(Context context, ReferralRemind referralRemind,
			String idString) {
		String whereClause = "_id = ? and user_name = ?";
		String[] whereArgs = { idString,
				"user" };
		ContentValues contentValues = new ContentValues();
		contentValues.put("alarmID", referralRemind.alarmID);
		contentValues.put("referral_title", referralRemind.referral_title);
		contentValues.put("remind_place", referralRemind.remind_place);
		contentValues.put("start_date", referralRemind.start_date);
		contentValues.put("remind_time", referralRemind.remind_time);
		contentValues.put("remarks", referralRemind.remarks);
		db.update(AlarmDBHelper.TB_REFERRAL_REMINDS, contentValues,
				whereClause, whereArgs);
	}

	public ArrayList<ReferralRemind> query() {
		ArrayList<ReferralRemind> referralReminds = new ArrayList<ReferralRemind>();
		Cursor c = queryTheCursor();
		if (c != null && c.getCount() > 0) {

			while (c.moveToNext()) {
				ReferralRemind referralRemind = new ReferralRemind();
				referralRemind._id = c.getInt(c.getColumnIndex("_id"));
				referralRemind.referral_title = c.getString(c
						.getColumnIndex("referral_title"));
				referralRemind.remind_time = c.getString(c
						.getColumnIndex("remind_time"));
				referralRemind.remind_place = c.getString(c
						.getColumnIndex("remind_place"));
				referralRemind.alarmID = c.getString(c
						.getColumnIndex("alarmID"));
				referralRemind.start_date = c.getString(c
						.getColumnIndex("start_date"));
				referralRemind.remarks = c.getString(c
						.getColumnIndex("remarks"));
				referralReminds.add(referralRemind);
				Log.i("Gao", "id is " + referralRemind._id);
			}
		}
		c.close();
		return referralReminds;

	}

	public ArrayList<ReferralRemind> queryDate(String start_date) {
		ArrayList<ReferralRemind> referralReminds = new ArrayList<ReferralRemind>();
		Cursor c = queryAsDate(start_date);
		if (c != null && c.getCount() > 0) {

			while (c.moveToNext()) {
				ReferralRemind referralRemind = new ReferralRemind();
				referralRemind._id = c.getInt(c.getColumnIndex("_id"));
				referralRemind.referral_title = c.getString(c
						.getColumnIndex("referral_title"));
				referralRemind.remind_time = c.getString(c
						.getColumnIndex("remind_time"));
				referralRemind.remind_place = c.getString(c
						.getColumnIndex("remind_place"));
				referralRemind.alarmID = c.getString(c
						.getColumnIndex("alarmID"));
				referralRemind.start_date = c.getString(c
						.getColumnIndex("start_date"));
				referralRemind.remarks = c.getString(c
						.getColumnIndex("remarks"));
				referralReminds.add(referralRemind);
				Log.i("Gao", "id is " + referralRemind._id);
			}
		}
		c.close();
		return referralReminds;

	}

	public ArrayList<ReferralRemind> queryAll() {
		ArrayList<ReferralRemind> referralReminds = new ArrayList<ReferralRemind>();
		Cursor c = queryTheCursor();
		if (c != null && c.getCount() > 0) {

			while (c.moveToNext()) {
				ReferralRemind referralRemind = new ReferralRemind();
				referralRemind._id = c.getInt(c.getColumnIndex("_id"));
				referralRemind.referral_title = c.getString(c
						.getColumnIndex("referral_title"));
				referralRemind.remind_place = c.getString(c
						.getColumnIndex("remind_place"));
				referralRemind.start_date = c.getString(c
						.getColumnIndex("start_date"));
				referralRemind.remind_time = c.getString(c
						.getColumnIndex("remind_time"));
				referralRemind.remarks = c.getString(c
						.getColumnIndex("remarks"));
				referralRemind.alarmID = c.getString(c
						.getColumnIndex("alarmID"));

				referralReminds.add(referralRemind);
				Log.i("Gao", "id is " + referralRemind._id);
			}
		}
		c.close();
		return referralReminds;
	}

	public ReferralRemind addToService() {
		Cursor cursor = queryTheCursor();
		cursor.moveToLast();
		ReferralRemind referralRemind = new ReferralRemind();
		referralRemind._id = cursor.getInt(cursor.getColumnIndex("_id"));
		referralRemind.referral_title = cursor.getString(cursor
				.getColumnIndex("referral_title"));
		referralRemind.remind_place = cursor.getString(cursor
				.getColumnIndex("remind_place"));
		referralRemind.start_date = cursor.getString(cursor
				.getColumnIndex("start_date"));
		referralRemind.remind_time = cursor.getString(cursor
				.getColumnIndex("remind_time"));
		referralRemind.remarks = cursor.getString(cursor
				.getColumnIndex("remarks"));
		referralRemind.alarmID = cursor.getString(cursor
				.getColumnIndex("alarmID"));
		cursor.close();

		return referralRemind;

	}

	private static Cursor queryTheCursor() {
		Cursor c = db.rawQuery(
				"SELECT * FROM referral_remind WHERE user_name = ?",
				new String[] { "user" });
		return c;
	}

	private static Cursor queryAsDate(String start_date) {
		Cursor c = db
				.rawQuery(
						"SELECT * FROM referral_remind WHERE user_name = ? and start_date = ? order by remind_time",
						new String[] {
								"user",
								start_date });
		return c;
	}

	public void closeDB() {
		db.close();
	}

}
