package com.gaosir.www.healthassisant.risk_inspect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gaosir.www.healthassisant.R;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-6-16
 */
public class AnxietyEvaluate extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    // TODO 改写焦虑模型
    private RadioGroup[] answerchoice;
    private TextView title, adaptcrowd;
    private Button submit;
    private ImageView back;
    private String reportcontentone;
    private int[] scores = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1,};
    boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.anxiety_evaluate);
        Log.i("Gao", "init AnxietyEvaluate");
        init();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                back.setBackgroundColor(Color.argb(80, 255, 255, 255));
                finish();
                break;
            case R.id.submit:

                String risk_result = "";

                for (int i = 0; i < answerchoice.length; i++) {

                    if (scores[i] == -1) {
                        Toast.makeText(getApplicationContext(),
                                "第" + i + 1 + "项没有填", Toast.LENGTH_LONG).show();
                        break;
                    }
                    if (i == 13)
                        flag = true;
                }
                if (!flag)
                    Toast.makeText(getApplicationContext(), "题目没有答完",
                            Toast.LENGTH_LONG).show();
                else {
                    int score = 0;
                    for (int i = 0; i < answerchoice.length; i++) {
                        score += scores[i];
                    }
                    risk_result = getResult(score);
                    String post_data = reportcontentone + risk_result;
                    String remark = getResources().getString(
                            R.string.report_remark_3);
                    showResultDialog(post_data, remark);
                    System.out.print("Gao");
                }
                break;
            default:
                break;
        }
    }

    public void showResultDialog(String result, String remark) {
        final Dialog dialog = new Dialog(AnxietyEvaluate.this, R.style.dialog);
        dialog.setContentView(R.layout.dialog_evaluate_report);
        TextView TVResult = (TextView) dialog.findViewById(R.id.tv_evaluate_result);
        TextView TVRemark = (TextView) dialog.findViewById(R.id.tv_remark);
        TVRemark.setText(remark);
        TVResult.setText(result);
        Button IKnown = (Button) dialog.findViewById(R.id.btn_i_know);
        IKnown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }


    private String getResult(int score) {
        String result = "";
        if (score >= 0 && score <= 7) {
            result = "无抑郁或者焦虑";
        } else if (score >= 8 && score <= 10) {
            result = "有轻度抑郁或焦虑";
        } else if (score >= 11 && score <= 14) {
            result = "有中度抑郁或焦虑";
        } else if (score >= 15) {
            result = "有严重抑郁或焦虑";
        }
        return result;
    }

    private void init() {
        Intent intent = getIntent();
        String titletext = intent.getStringExtra("title");
        String adapttext = intent.getStringExtra("adapt_crowd");
        reportcontentone = intent.getStringExtra("report_content_one");
        title = (TextView) findViewById(R.id.title);
        title.setText(titletext);
        adaptcrowd = (TextView) findViewById(R.id.adaptcrowd);
        adaptcrowd.setText(adapttext);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        answerchoice = new RadioGroup[]{
                (RadioGroup) findViewById(R.id.group0),
                (RadioGroup) findViewById(R.id.group1),
                (RadioGroup) findViewById(R.id.group2),
                (RadioGroup) findViewById(R.id.group3),
                (RadioGroup) findViewById(R.id.group4),
                (RadioGroup) findViewById(R.id.group5),
                (RadioGroup) findViewById(R.id.group6),
                (RadioGroup) findViewById(R.id.group7),
                (RadioGroup) findViewById(R.id.group8),
                (RadioGroup) findViewById(R.id.group9),
                (RadioGroup) findViewById(R.id.group10),
                (RadioGroup) findViewById(R.id.group11),
                (RadioGroup) findViewById(R.id.group12),
                (RadioGroup) findViewById(R.id.group13),};
        for (int i = 0; i < answerchoice.length; i++) {
            answerchoice[i].setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton selected = (RadioButton) findViewById(group
                .getCheckedRadioButtonId());
        String text = selected.getText().toString();
        switch (group.getId()) {
            case R.id.group0:
                if (text.contains("A")) {
                    scores[0] = 3;
                } else if (text.contains("B")) {
                    scores[0] = 2;
                } else if (text.contains("C")) {
                    scores[0] = 1;
                } else if (text.contains("D")) {
                    scores[0] = 0;
                }
                break;
            case R.id.group1:
                if (text.contains("A")) {
                    scores[1] = 0;
                } else if (text.contains("B")) {
                    scores[1] = 1;
                } else if (text.contains("C")) {
                    scores[1] = 2;
                } else if (text.contains("D")) {
                    scores[1] = 3;
                }
                break;
            case R.id.group2:
                if (text.contains("A")) {
                    scores[2] = 3;
                } else if (text.contains("B")) {
                    scores[2] = 2;
                } else if (text.contains("C")) {
                    scores[2] = 1;
                } else if (text.contains("D")) {
                    scores[2] = 0;
                }
                break;
            case R.id.group3:
                if (text.contains("A")) {
                    scores[3] = 0;
                } else if (text.contains("B")) {
                    scores[3] = 1;
                } else if (text.contains("C")) {
                    scores[3] = 2;
                } else if (text.contains("D")) {
                    scores[3] = 3;
                }
                break;
            case R.id.group4:
                if (text.contains("A")) {
                    scores[4] = 3;
                } else if (text.contains("B")) {
                    scores[4] = 2;
                } else if (text.contains("C")) {
                    scores[4] = 1;
                } else if (text.contains("D")) {
                    scores[4] = 0;
                }
                break;
            case R.id.group5:
                if (text.contains("A")) {
                    scores[5] = 3;
                } else if (text.contains("B")) {
                    scores[5] = 2;
                } else if (text.contains("C")) {
                    scores[5] = 1;
                } else if (text.contains("D")) {
                    scores[5] = 0;
                }
                break;
            case R.id.group6:
                if (text.contains("A")) {
                    scores[6] = 0;
                } else if (text.contains("B")) {
                    scores[6] = 1;
                } else if (text.contains("C")) {
                    scores[6] = 2;
                } else if (text.contains("D")) {
                    scores[6] = 3;
                }
                break;
            case R.id.group7:
                if (text.contains("A")) {
                    scores[7] = 3;
                } else if (text.contains("B")) {
                    scores[7] = 2;
                } else if (text.contains("C")) {
                    scores[7] = 1;
                } else if (text.contains("D")) {
                    scores[7] = 0;
                }
                break;
            case R.id.group8:
                if (text.contains("A")) {
                    scores[8] = 0;
                } else if (text.contains("B")) {
                    scores[8] = 1;
                } else if (text.contains("C")) {
                    scores[8] = 2;
                } else if (text.contains("D")) {
                    scores[8] = 3;
                }
                break;
            case R.id.group9:
                if (text.contains("A")) {
                    scores[9] = 3;
                } else if (text.contains("B")) {
                    scores[9] = 2;
                } else if (text.contains("C")) {
                    scores[9] = 1;
                } else if (text.contains("D")) {
                    scores[9] = 0;
                }
                break;
            case R.id.group10:
                if (text.contains("A")) {
                    scores[10] = 3;
                } else if (text.contains("B")) {
                    scores[10] = 2;
                } else if (text.contains("C")) {
                    scores[10] = 1;
                } else if (text.contains("D")) {
                    scores[10] = 0;
                }
                break;
            case R.id.group11:
                if (text.contains("A")) {
                    scores[11] = 0;
                } else if (text.contains("B")) {
                    scores[11] = 1;
                } else if (text.contains("C")) {
                    scores[11] = 2;
                } else if (text.contains("D")) {
                    scores[11] = 3;
                }
                break;
            case R.id.group12:
                if (text.contains("A")) {
                    scores[12] = 3;
                } else if (text.contains("B")) {
                    scores[12] = 2;
                } else if (text.contains("C")) {
                    scores[12] = 1;
                } else if (text.contains("D")) {
                    scores[12] = 0;
                }
                break;
            case R.id.group13:

                if (text.contains("A")) {
                    scores[13] = 0;
                } else if (text.contains("B")) {
                    scores[13] = 1;
                } else if (text.contains("C")) {
                    scores[13] = 2;
                } else if (text.contains("D")) {
                    scores[13] = 3;
                }
                break;
            default:
                break;
        }
    }

}
