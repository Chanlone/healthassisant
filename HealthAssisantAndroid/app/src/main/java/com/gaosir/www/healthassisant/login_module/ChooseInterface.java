package com.gaosir.www.healthassisant.login_module;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.TabHostOld;

/**
 * @author Chanlone Gogh
 * @version 1.1.0 2016-8-9
 */
public class ChooseInterface extends Activity implements View.OnClickListener {
    public static final String LOGIN_INTER = "login";
    public static final String GUEST_INTER = "guest";
    private static final String mTAG = "CI";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.choose_interface);
        findViewById(R.id.tv_user_login).setOnClickListener(this);
        findViewById(R.id.tv_guest_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.tv_user_login:
                intent.setClass(this, Login.class);
                intent.putExtra("interface", LOGIN_INTER);
                Log.i(mTAG, "click 1");
                break;
            case R.id.tv_guest_login:
                SharedPreferences user_info = getSharedPreferences("user_info", 0);
                SharedPreferences.Editor editor = user_info.edit();
                editor.putString("name", "guest_user");
                editor.apply();
                intent.setClass(this, TabHostOld.class);
                intent.putExtra("interface", GUEST_INTER);
                Log.i(mTAG, "click 2");
                break;
        }
        startActivity(intent);
    }
}
