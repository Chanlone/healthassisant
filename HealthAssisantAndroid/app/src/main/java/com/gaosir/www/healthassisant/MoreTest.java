package com.gaosir.www.healthassisant;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.avos.avoscloud.AVObject;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ItemData;

/**
 * @author Chanlone Gogh
 * @version 0.2.3 2016/8/15.
 */
public class MoreTest extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.more);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            //TODO 检查更新的方法
            case R.id.rl_check_update:
                break;
            //TODO 添加反馈的方法
            case R.id.rl_feed_back:
                break;
            //TODO 两个测试模块，心率，智能导诊的模块
            case R.id.rl_test_module:
                break;
            //TODO 提醒设置
            case R.id.rl_remind_setting:
                break;
        }
    }
    private void addFeedbackToCloud(ItemData itemData){
        AVObject feedback = new AVObject("FeedBack");
    }
}
