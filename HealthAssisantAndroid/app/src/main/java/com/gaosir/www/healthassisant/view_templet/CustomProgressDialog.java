package com.gaosir.www.healthassisant.view_templet;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaosir.www.healthassisant.R;

/**
 * @author Chanlone Gogh
 * @version 1.1.0 2016/8/10.
 */
public class CustomProgressDialog extends Dialog {
    private Context mContext = null;
    private static CustomProgressDialog mCustomProgressDialog = null;

    public CustomProgressDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    public CustomProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static CustomProgressDialog createDialog(Context context) {
        mCustomProgressDialog = new CustomProgressDialog(context, R.style.CustomProgressDialog);
        mCustomProgressDialog.setContentView(R.layout.customprogressdialog);
        mCustomProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        return mCustomProgressDialog;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (mCustomProgressDialog == null) {
            return;
        }

        ImageView imageView = (ImageView) mCustomProgressDialog.findViewById(R.id.loadingImageView);
        imageView.setBackgroundResource(R.drawable.login_drawble);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();
    }

    //    public CustomProgressDialog setTitle(String strTitle){
//        return mCustomProgressDialog;
//    }
    public CustomProgressDialog setMessage(String strMessage) {
        TextView tvMsg = (TextView) mCustomProgressDialog.findViewById(R.id.id_tv_loadingmsg);

        if (tvMsg != null) {
            tvMsg.setText(strMessage);
        }
        return mCustomProgressDialog;
    }
}
