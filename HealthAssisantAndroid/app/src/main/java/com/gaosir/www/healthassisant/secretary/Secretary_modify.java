package com.gaosir.www.healthassisant.secretary;
/**
 * @author Chanlone
 * @version 1.0.3
 * @time 2015.6.3
 * @email shadowkael@gmail.com
 */

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.AlarmDBManager;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.MedicalRemind;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ReferralDBManager;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ReferralRemind;
import com.gaosir.www.healthassisant.data_format_tools.DpTransformPx;
import com.gaosir.www.healthassisant.data_format_tools.SecretaryDateTimeFormat;
import com.gaosir.www.healthassisant.login_module.ChooseInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Secretary_modify extends Activity {

    private final static String TYPE_MEDICAL = "medical";
    private final static String TYPE_REFERRAL = "referral";
    private final static int DAY2MILLIS = 24 * 60 * 60 * 1000;
    private final static int LIST_ITEM_HIGHT = 48;
    private int flag = 0;
    private int tempDate = 0;
    private int alarmID = 0;

    private LinearLayout symbol;
    private LinearLayout remind_content;
    private LinearLayout null_remind_layout;
    private TextView yesterdayView;
    private TextView tomorrowView;
    private TextView currentDateView;
    private TextView todayView;
    private ImageButton addRemindView;
    private ImageButton back;
    private Button addButton;

    TextView timeView;
    private Calendar calendar;
    private Long current_date;

    private ReferralDBManager mReferralDBManager = null;
    private AlarmDBManager mAlarmDBManager = null;

    private ArrayList<Map<String, String>> list_remind = null;
    private ArrayList<ArrayList<Map<String, String>>> globleList = null;

    private Comparator<Map<String, String>> comparator = new Comparator<Map<String, String>>() {

        @Override
        public int compare(Map<String, String> lhs, Map<String, String> rhs) {

            int map1value = Integer.valueOf(lhs.get("remind_time").replace(":",
                    ""));
            int map2value = Integer.valueOf(rhs.get("remind_time").replace(":",
                    ""));
            return map1value - map2value;

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.patient_secretary);

        back = (ImageButton) findViewById(R.id.btn_back_top);
        addRemindView = (ImageButton) findViewById(R.id.btn_add_remind_top);
        addButton = (Button) findViewById(R.id.btn_add_remind);

        symbol = (LinearLayout) findViewById(R.id.symbol_icon);
        remind_content = (LinearLayout) findViewById(R.id.reminder_content);
        yesterdayView = (TextView) findViewById(R.id.tv_left_yesterday);
        tomorrowView = (TextView) findViewById(R.id.tv_right_tomorrow);
        todayView = (TextView) findViewById(R.id.tv_today);
        currentDateView = (TextView) findViewById(R.id.tv_middle_date);
        null_remind_layout = (LinearLayout) findViewById(R.id.null_remind_layout);

        initView();
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<");

        Intent it = getIntent();
        Log.i("Gao", "alarmID:" + it.getIntExtra("alarmID", 0));
    }

    @Override
    protected void onResume() {

        super.onResume();
        flag = 0;
        list_remind.clear();
        loadReferralList();
        loadMedicalList();
        Collections.sort(list_remind, comparator);
        if (list_remind != null && list_remind.size() > 0) {
            null_remind_layout.setVisibility(View.INVISIBLE);
        } else {
            null_remind_layout.setVisibility(View.VISIBLE);
        }
        symbol.removeAllViews();
        remind_content.removeAllViews();
        symbol.postInvalidate();
        remind_content.postInvalidate();
        initListLayout();
    }

    public void initView() {

        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        currentDateView.setText(SecretaryDateTimeFormat
                .getDisplayDate(calendar));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        alarmID = (int) (calendar.getTimeInMillis() / 1000 / 60 / 60 / 24);

        initValue();
        if (list_remind != null && list_remind.size() > 0) {
            null_remind_layout.setVisibility(View.INVISIBLE);
        }

        initListLayout();

        back.setOnClickListener(ls);
        yesterdayView.setOnClickListener(ls);
        tomorrowView.setOnClickListener(ls);
        addRemindView.setOnClickListener(ls);
        addButton.setOnClickListener(ls);
        todayView.setOnClickListener(ls);

    }

    OnClickListener ls = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_back_top:
//				finish();
//				Intent intent = new Intent();
//				intent.setClass(getApplicationContext(), HeartRateMonitor.class);
//				startActivity(intent);
//				Intent intent = new Intent(Secretary_modify.this, DiseaseRisk.class);
                    Intent intent = new Intent(Secretary_modify.this, ChooseInterface.class);
                    startActivity(intent);
                    break;
                case R.id.tv_left_yesterday:
                    yestertday();

                    break;
                case R.id.tv_right_tomorrow:
                    tomorrow();

                    break;

                case R.id.btn_add_remind_top:
                    alertDialog();

                    break;
                case R.id.btn_add_remind:
                    alertDialog();

                    break;
                case R.id.tv_today:
                    Log.i("Gao", "today");
                    today();
                    // Intent secretary_modify_2 = new Intent();
                    // secretary_modify_2.setClass(getApplicationContext(),
                    // com.xinyue.patient.Secretary_modify_2.class);
                    // startActivity(secretary_modify_2);
                    break;

                default:
                    break;
            }

        }
    };

    AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {

        @Override
        public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
            @SuppressWarnings("unchecked")
            HashMap<String, String> map = (HashMap<String, String>) arg0
                    .getItemAtPosition(arg2);
            String idString = map.get("_id");
            String alarmArg = "";
            String remind_type = map.get("remind_type");
            int[] alarmIDs = {0, 0, 0};
            int index = Integer.valueOf(map.get("list_remind_index"));
            if (remind_type.equals(TYPE_REFERRAL)) {
                alarmArg = map.get("alarmID");
                confirmOperate(arg2, idString, alarmArg, remind_type, index, alarmIDs);
            } else {
                alarmIDs[0] = Integer.valueOf(map.get("alarmID_1"));
                alarmIDs[1] = Integer.valueOf(map.get("alarmID_2"));
                alarmIDs[2] = Integer.valueOf(map.get("alarmID_3"));
                confirmOperate(arg2, idString, "0", remind_type, index, alarmIDs);
            }

            return true;
        }

        private void confirmOperate(final int arg2, final String idString,
                                    final String alarmArg, final String remind_type, final int index, final int[] alarmIDs) {
            AlertDialog.Builder builder = new Builder(Secretary_modify.this);
            builder.setTitle(R.string.confirm_delete_message);
            builder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (remind_type.equals(TYPE_REFERRAL)) {
                                mReferralDBManager.deleteData(
                                        Secretary_modify.this, idString);

                                AlarmManager alarmManager = (AlarmManager)
                                        getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(PendingIntent.getBroadcast(
                                        getApplicationContext(), Integer
                                                .valueOf(alarmArg), new Intent(
                                                getApplicationContext(),
                                                AlarmReceiver.class), 0));
                            }
                            if (remind_type.equals(TYPE_MEDICAL)) {
                                mAlarmDBManager.deleteData(
                                        Secretary_modify.this, idString);
                                for (int i : alarmIDs) {

                                    AlarmManager alarmManager = (AlarmManager)
                                            getSystemService(Context.ALARM_SERVICE);
                                    alarmManager.cancel(PendingIntent.getBroadcast(
                                            getApplicationContext(), i, new Intent(
                                                    getApplicationContext(),
                                                    AlarmReceiver.class), 0));
                                }

                            }
                            System.out.println(list_remind);
                            Log.i("Gao", "index:" + index);

                            list_remind.clear();
                            loadReferralList();
                            loadMedicalList();
                            Collections.sort(list_remind, comparator);
                            if (list_remind != null && list_remind.size() > 0) {
                                null_remind_layout
                                        .setVisibility(View.INVISIBLE);
                            } else {
                                null_remind_layout.setVisibility(View.VISIBLE);
                            }
                            symbol.removeAllViews();
                            remind_content.removeAllViews();
                            symbol.postInvalidate();
                            remind_content.postInvalidate();
                            initListLayout();
                        }
                    });
            builder.setNegativeButton(R.string.cancel, null);
            builder.create().show();

        }
    };

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                long arg3) {
            @SuppressWarnings("unchecked")
            HashMap<String, String> map = (HashMap<String, String>) arg0
                    .getItemAtPosition(arg2);
            String idString = map.get("_id");
            String remind_type = map.get("remind_type");
            if (remind_type.equals(TYPE_REFERRAL)) {

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),
                        AddReferralAlarm.class);
                intent.putExtra("alarmID", map.get("alarmID"));
                intent.putExtra("_id", idString);
                intent.putExtra("referral_title", map.get("remind_name"));
                intent.putExtra("remind_place", map.get("remind_extra"));
                intent.putExtra("start_date", map.get("start_date"));
                intent.putExtra("remind_time", map.get("remind_time"));
                intent.putExtra("remarks", map.get("remarks"));
                intent.putExtra("type", "edit");

                startActivity(intent);
            } else {

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),
                        AddMedicineAlarm.class);
                intent.putExtra("alarmID_1", map.get("alarmID_1"));
                intent.putExtra("alarmID_2", map.get("alarmID_2"));
                intent.putExtra("alarmID_3", map.get("alarmID_3"));
                intent.putExtra("_id", idString);
                intent.putExtra("medical_name", map.get("remind_name"));
                intent.putExtra("dosage", map.get("remind_extra"));
                intent.putExtra("start_date", map.get("start_date"));
                intent.putExtra("end_date", map.get("end_date"));
                intent.putExtra("remind_morning", map.get("remind_morning"));
                intent.putExtra("remind_noon", map.get("remind_noon"));
                intent.putExtra("remind_evening", map.get("remind_evening"));
                intent.putExtra("repeat_type", map.get("repeat_type"));
                intent.putExtra("advance", map.get("advance"));
                intent.putExtra("remarks", map.get("remarks"));
                intent.putExtra("type", "edit");

                startActivity(intent);

            }

            // int alarmID = Integer.valueOf(alarmArg);
            Log.i("Gao", "longClick_ID :" + idString);

        }
    };

    public void initListLayout() {
        String currentDate = "";
        Calendar tCalendar = Calendar.getInstance();
        tCalendar.setTimeInMillis(System.currentTimeMillis());
        currentDate = SecretaryDateTimeFormat.getDisplayDate(tCalendar);

        if (currentDateView.getText().toString().equals(currentDate)) {
            todayView.setVisibility(View.INVISIBLE);
        } else {
            todayView.setVisibility(View.VISIBLE);
        }

        for (int i = 0, index = 0; i < list_remind.size(); i++) {
            ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
            list.add(list_remind.get(i));
            int next = i + 1;
            for (int j = next; j < list_remind.size(); j++) {

                if (list_remind.get(i).get("remind_time")
                        .equals(list_remind.get(j).get("remind_time"))
                        && list_remind.get(i).get("remind_type")
                        .equals(list_remind.get(j).get("remind_type"))) {
                    list.add(list_remind.get(j));
                    list_remind.remove(j);

                }
            }
            System.out.println(list);
            globleList.add(list);
            // add gray line_1
            ImageView bottom_gray_line_1 = new ImageView(this);
            bottom_gray_line_1.setBackgroundColor(getResources().getColor(
                    R.color.secretary_line_middle));

            LinearLayout.LayoutParams gray_lineParams_1 = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, DpTransformPx.dp2px(this, 1));
            gray_lineParams_1
                    .setMargins(0, DpTransformPx.dp2px(this, 26), 0, 0);
            bottom_gray_line_1.setLayoutParams(gray_lineParams_1);
            remind_content.addView(bottom_gray_line_1);

            // set listView
            int listDp = list.size() * LIST_ITEM_HIGHT + list.size() - 1;
            ListView contListView = new ListView(this);
            SimpleAdapter adapter = new SimpleAdapter(this, list,
                    R.layout.secretary_remind_item, new String[]{
                    "remind_name", "remind_extra"}, new int[]{
                    R.id.remind_content_1, R.id.remind_content_2});
            contListView.setAdapter(adapter);

            // add listView to layout.

            LinearLayout.LayoutParams contentParams = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    DpTransformPx.dp2px(this, listDp));
            contListView.setLayoutParams(contentParams);

            remind_content.addView(contListView);

            // add the blank area.
            // add gray_line_2.

            ImageView bottom_gray_line_2 = new ImageView(this);
            bottom_gray_line_2.setBackgroundColor(getResources().getColor(
                    R.color.secretary_line_middle));
            LinearLayout.LayoutParams gray_lineParams_2 = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, DpTransformPx.dp2px(this, 1));
            bottom_gray_line_2.setLayoutParams(gray_lineParams_2);

            remind_content.addView(bottom_gray_line_2);

            // add symbol
            ImageView symbol_pic = new ImageView(this);
            if (list.get(0).get("remind_type").equals(TYPE_MEDICAL)) {
                symbol_pic.setImageResource(R.drawable.remind_medical_1);
            }
            if (list.get(0).get("remind_type").equals(TYPE_REFERRAL)) {
                symbol_pic.setImageResource(R.drawable.remind_referral_1);
            }
            LinearLayout.LayoutParams symbolParams = new LinearLayout.LayoutParams(
                    DpTransformPx.dp2px(this, 48),
                    DpTransformPx.dp2px(this, 48));
            symbol_pic.setLayoutParams(symbolParams);
            symbol.addView(symbol_pic);

            // add time

            TextView timeView = new TextView(this);
            timeView.setBackgroundColor(getResources().getColor(
                    R.color.secretary_time_bg));
            timeView.setText(list.get(0).get("remind_time"));
            timeView.setTextColor(getResources().getColor(
                    R.color.bottom_text_color_normal));
            LinearLayout.LayoutParams timeParams = new LinearLayout.LayoutParams(
                    DpTransformPx.dp2px(this, 40),
                    DpTransformPx.dp2px(this, 20));
            timeParams.setMargins(0, DpTransformPx.dp2px(this, 4), 0,
                    DpTransformPx.dp2px(this, 4));
            timeView.setLayoutParams(timeParams);
            timeView.setGravity(Gravity.CENTER);
            symbol.addView(timeView);

            // add gray bar if it is needed.
            if (list.size() > 1) {
                ImageView gray_bar = new ImageView(this);
                int lenth = (list.size() - 1) * LIST_ITEM_HIGHT + list.size()
                        - 1 - 4;
                gray_bar.setBackgroundColor(getResources().getColor(
                        R.color.secretary_line_middle));
                LinearLayout.LayoutParams gray_barParams = new LinearLayout.LayoutParams(
                        DpTransformPx.dp2px(this, 4), DpTransformPx.dp2px(this,
                        lenth));
                gray_barParams
                        .setMargins(0, 0, 0, DpTransformPx.dp2px(this, 4));
                gray_bar.setLayoutParams(gray_barParams);
                symbol.addView(gray_bar);

            }
            // add clickListener

            contListView.setOnItemClickListener(onItemClickListener);
            contListView.setOnItemLongClickListener(onItemLongClickListener);

        }

    }

    public void initValue() {
        current_date = calendar.getTimeInMillis();

        mAlarmDBManager = new AlarmDBManager(this);
        mReferralDBManager = new ReferralDBManager(this);

        list_remind = new ArrayList<Map<String, String>>();
        globleList = new ArrayList<ArrayList<Map<String, String>>>();
        loadReferralList();
//        Log.i("Gao", "List size:" + list_remind.size());
        loadMedicalList();
        Collections.sort(list_remind, comparator);
//        Log.i("Gao", "List size:" + list_remind.size());

    }

    private void loadReferralList() {
        List<ReferralRemind> referralReminds = mReferralDBManager.query();
        for (ReferralRemind referralRemind : referralReminds) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("_id", "" + referralRemind._id);
            map.put("alarmID", referralRemind.alarmID);
            map.put("remind_type", TYPE_REFERRAL);
            map.put("remind_name", referralRemind.referral_title);
            map.put("remind_extra", referralRemind.remind_place);
            map.put("start_date", referralRemind.start_date);
            map.put("remind_time", referralRemind.remind_time);
            // map.put("repeat_type", referralRemind.repeat_type);
            // map.put("repeat_times", referralRemind.repeat_times);
            map.put("remarks", referralRemind.remarks);
            map.put("list_remind_index", "" + list_remind.size());
            // list_remind.add(map);

//            Log.i("Gao", "current mill" + current_date);
//            Log.i("Gao", "item mill" + calendar.getTimeInMillis());

            // add condition of only remind once.
            long temp_start_date = 0;
            if ((referralRemind.start_date.equals("") || referralRemind.start_date == null)) {
                continue;
            } else {
                calendar.set(
                        SecretaryDateTimeFormat
                                .getSeparatedDate(referralRemind.start_date)[0],
                        SecretaryDateTimeFormat
                                .getSeparatedDate(referralRemind.start_date)[1],
                        SecretaryDateTimeFormat
                                .getSeparatedDate(referralRemind.start_date)[2],
                        0, 0, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                temp_start_date = calendar.getTimeInMillis();
            }
            if (temp_start_date != current_date) {
                continue;
            }
            list_remind.add(map);

        }
//        Log.i("Gao", "List size:" + list_remind.size());

    }

    private void loadMedicalList() {

        List<MedicalRemind> medicalReminds = mAlarmDBManager.query();
        for (MedicalRemind medicalRemind : medicalReminds) {
            HashMap<String, String> map = new HashMap<String, String>();

            map.put("_id", "" + medicalRemind._id);
            map.put("alarmID_1", medicalRemind.alarmID_1);
            map.put("alarmID_2", medicalRemind.alarmID_2);
            map.put("alarmID_3", medicalRemind.alarmID_3);
            map.put("remind_type", TYPE_MEDICAL);
            map.put("remind_name", medicalRemind.medical_name);
            map.put("remind_extra", medicalRemind.dosage);
            map.put("start_date", medicalRemind.start_date);
            map.put("end_date", medicalRemind.end_date);
            map.put("remind_morning", medicalRemind.remind_morning);
            map.put("remind_noon", medicalRemind.remind_noon);
            map.put("remind_evening", medicalRemind.remind_evening);
            map.put("repeat_type", medicalRemind.repeat_type);
            map.put("advance", medicalRemind.advance);
            map.put("remarks", medicalRemind.remarks);
            map.put("list_remind_index", "" + list_remind.size());

            calendar.set(SecretaryDateTimeFormat
                            .getSeparatedDate(medicalRemind.start_date)[0],
                    SecretaryDateTimeFormat
                            .getSeparatedDate(medicalRemind.start_date)[1],
                    SecretaryDateTimeFormat
                            .getSeparatedDate(medicalRemind.start_date)[2], 0,
                    0, 0);
            long temp_start_date = calendar.getTimeInMillis();

            long temp_end_date = 0;

            if (!medicalRemind.end_date.equals("0")) {

                calendar.set(SecretaryDateTimeFormat
                                .getSeparatedDate(medicalRemind.end_date)[0],
                        SecretaryDateTimeFormat
                                .getSeparatedDate(medicalRemind.end_date)[1],
                        SecretaryDateTimeFormat
                                .getSeparatedDate(medicalRemind.end_date)[2],
                        0, 0, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                temp_end_date = calendar.getTimeInMillis();
            }

            if (!(medicalRemind.start_date.equals("") || medicalRemind.start_date == null)) {
                if (temp_end_date != 0
                        && (temp_start_date > current_date || current_date > temp_end_date)) {
                    continue;
                }

            } else {

                continue;
            }

            // judge current time whether in remind area.
            if (!medicalRemind.repeat_type.equals("0")) {

                if ((current_date - temp_start_date)
                        % (Long.valueOf(medicalRemind.repeat_type) * DAY2MILLIS) != 0) {
                    continue;
                }
            }
            // list_remind.add(map);
            if (!medicalRemind.remind_morning.equals("")) {
                map.put("remind_time", medicalRemind.remind_morning);
                list_remind.add(map);
            }
            if (!medicalRemind.remind_noon.equals("")) {
                HashMap<String, String> map2 = new HashMap<String, String>();
                map2.put("_id", "" + medicalRemind._id);
                map2.put("alarmID_1", medicalRemind.alarmID_1);
                map2.put("alarmID_2", medicalRemind.alarmID_2);
                map2.put("alarmID_3", medicalRemind.alarmID_3);
                map2.put("remind_type", TYPE_MEDICAL);
                map2.put("remind_name", medicalRemind.medical_name);
                map2.put("remind_extra", medicalRemind.dosage);
                map2.put("start_date", medicalRemind.start_date);
                map2.put("end_date", medicalRemind.end_date);
                map2.put("remind_morning", medicalRemind.remind_morning);
                map2.put("remind_noon", medicalRemind.remind_noon);
                map2.put("remind_evening", medicalRemind.remind_evening);
                map2.put("repeat_type", medicalRemind.repeat_type);
                map2.put("advance", medicalRemind.advance);
                map2.put("remarks", medicalRemind.remarks);
                map2.put("remind_time", medicalRemind.remind_noon);
                map2.put("list_remind_index", "" + list_remind.size());

                list_remind.add(map2);
            }
            if (!medicalRemind.remind_evening.equals("")) {
                HashMap<String, String> map3 = new HashMap<String, String>();
                map3.put("_id", "" + medicalRemind._id);
                map3.put("alarmID_1", medicalRemind.alarmID_1);
                map3.put("alarmID_2", medicalRemind.alarmID_2);
                map3.put("alarmID_3", medicalRemind.alarmID_3);
                map3.put("remind_type", TYPE_MEDICAL);
                map3.put("remind_name", medicalRemind.medical_name);
                map3.put("remind_extra", medicalRemind.dosage);
                map3.put("start_date", medicalRemind.start_date);
                map3.put("end_date", medicalRemind.end_date);
                map3.put("remind_morning", medicalRemind.remind_morning);
                map3.put("remind_noon", medicalRemind.remind_noon);
                map3.put("remind_evening", medicalRemind.remind_evening);
                map3.put("repeat_type", medicalRemind.repeat_type);
                map3.put("advance", medicalRemind.advance);
                map3.put("remarks", medicalRemind.remarks);
                map3.put("remind_time", medicalRemind.remind_noon);
                map3.put("remind_time", medicalRemind.remind_evening);
                map3.put("list_remind_index", "" + list_remind.size());

                list_remind.add(map3);
            }
        }
        System.out.println(list_remind);

    }

    public void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.choose_remind_type);
        builder.setItems(R.array.remind_type,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Log.i("Gao", "" + which);
                        switch (which) {
                            case 0:
                                Intent intent_medical = new Intent();
                                intent_medical.setClass(getApplicationContext(),
                                        AddMedicineAlarm.class);
                                intent_medical.putExtra("_id", "");
                                intent_medical.putExtra("medical_name", "");
                                intent_medical.putExtra("alarmID", "");
                                intent_medical.putExtra("type", "add");
                                startActivity(intent_medical);

                                break;
                            case 1:
                                Intent intent_referral = new Intent();
                                intent_referral.setClass(getApplicationContext(),
                                        AddReferralAlarm.class);
                                intent_referral.putExtra("_id", "");
                                intent_referral.putExtra("referral_title", "");
                                intent_referral.putExtra("referral_place", "");
                                intent_referral.putExtra("type", "add");
                                startActivity(intent_referral);

                                break;

                            default:
                                break;
                        }

                    }
                });
        // builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    public void yestertday() {
        flag = 0;
        calendar.setTimeInMillis(current_date);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        current_date = calendar.getTimeInMillis();
        currentDateView.setText(SecretaryDateTimeFormat
                .getDisplayDate(calendar));

        list_remind.clear();
        loadReferralList();
        loadMedicalList();
        if (list_remind != null && list_remind.size() > 0) {
            null_remind_layout.setVisibility(View.INVISIBLE);
        } else {
            null_remind_layout.setVisibility(View.VISIBLE);
        }
        Collections.sort(list_remind, comparator);
        symbol.removeAllViews();
        remind_content.removeAllViews();
        symbol.postInvalidate();
        remind_content.postInvalidate();
        initListLayout();
    }

    public void tomorrow() {
        flag = 0;
        calendar.setTimeInMillis(current_date);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        current_date = calendar.getTimeInMillis();
        currentDateView.setText(SecretaryDateTimeFormat
                .getDisplayDate(calendar));

        list_remind.clear();
        loadReferralList();
        loadMedicalList();
        if (list_remind != null && list_remind.size() > 0) {
            null_remind_layout.setVisibility(View.INVISIBLE);
        } else {
            null_remind_layout.setVisibility(View.VISIBLE);
        }
        Collections.sort(list_remind, comparator);
        symbol.removeAllViews();
        remind_content.removeAllViews();
        symbol.postInvalidate();
        remind_content.postInvalidate();
        initListLayout();
    }

    public void today() {
        flag = 0;
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        current_date = calendar.getTimeInMillis();
        currentDateView.setText(SecretaryDateTimeFormat
                .getDisplayDate(calendar));

        list_remind.clear();
        loadReferralList();
        loadMedicalList();
        if (list_remind != null && list_remind.size() > 0) {
            null_remind_layout.setVisibility(View.INVISIBLE);
        } else {
            null_remind_layout.setVisibility(View.VISIBLE);
        }
        Collections.sort(list_remind, comparator);
        symbol.removeAllViews();
        remind_content.removeAllViews();
        symbol.postInvalidate();
        remind_content.postInvalidate();
        initListLayout();
//        Log.i("Gao", "today completed");
    }

}
