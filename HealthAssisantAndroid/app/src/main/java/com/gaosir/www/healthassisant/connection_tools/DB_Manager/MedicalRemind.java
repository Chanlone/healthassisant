package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

public class MedicalRemind {
	public int _id;
	public String user_name = null;
	public String medical_name = null;
	public String dosage = null;
	public String start_date = null;
	public String end_date = null;
	public String remind_morning = null;
	public String remind_noon = null;
	public String remind_evening = null;
	public String repeat_type = null;
	public String advance;
	public String remarks = null;
	public String alarmID_1 = null;
	public String alarmID_2 = null;
	public String alarmID_3 = null;

	public String getAlarmID_1() {
		return alarmID_1;
	}

	public void setAlarmID_1(String alarmID_1) {
		this.alarmID_1 = alarmID_1;
	}

	public String getAlarmID_2() {
		return alarmID_2;
	}

	public void setAlarmID_2(String alarmID_2) {
		this.alarmID_2 = alarmID_2;
	}

	public String getAlarmID_3() {
		return alarmID_3;
	}

	public void setAlarmID_3(String alarmID_3) {
		this.alarmID_3 = alarmID_3;
	}

	public MedicalRemind() {

	}

	public int get_id() {
		return _id;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getMedical_name() {
		return medical_name;
	}

	public void setMedical_name(String medical_name) {
		this.medical_name = medical_name;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getRemind_morning() {
		return remind_morning;
	}

	public void setRemind_morning(String remind_morning) {
		this.remind_morning = remind_morning;
	}

	public String getRemind_noon() {
		return remind_noon;
	}

	public void setRemind_noon(String remind_noon) {
		this.remind_noon = remind_noon;
	}

	public String getRemind_evening() {
		return remind_evening;
	}

	public void setRemind_evening(String remind_evening) {
		this.remind_evening = remind_evening;
	}

	public String getRepeat_type() {
		return repeat_type;
	}

	public void setRepeat_type(String repeat_type) {
		this.repeat_type = repeat_type;
	}

	public String getAdvance() {
		return advance;
	}

	public void setAdvance(String advance) {
		this.advance = advance;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public MedicalRemind(String user_name, String medical_name, String dosage,
						 String start_date, String end_date, String remind_morning,
						 String remind_noon, String remind_evening, String repeat_type,
						 String advance, String remarks, String alarmID_1, String alarmID_2,
						 String alarmID_3) {
		this.user_name = user_name;
		this.medical_name = medical_name;
		this.dosage = dosage;
		this.start_date = start_date;
		this.end_date = end_date;
		this.remind_morning = remind_morning;
		this.remind_noon = remind_noon;
		this.remind_evening = remind_evening;
		this.repeat_type = repeat_type;
		this.advance = advance;
		this.remarks = remarks;
		this.alarmID_1 = alarmID_1;
		this.alarmID_2 = alarmID_2;
		this.alarmID_3 = alarmID_3;
	}

}