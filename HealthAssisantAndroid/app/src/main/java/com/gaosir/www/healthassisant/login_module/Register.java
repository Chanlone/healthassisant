package com.gaosir.www.healthassisant.login_module;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.SaveCallback;
import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.view_templet.CustomProgressDialog;

/**
 * @author Chanlone Gogh
 * @version 1.1.0 2016/8/9.
 */
public class Register extends Activity implements View.OnClickListener {

    private static final String mTAG = "Register";

    private EditText mUserName;
    private EditText mPassword;
    private EditText mNumber;
    private Button mRegister;

    private String mUser;
    private String mPass;
    private String mNum;

    CustomProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.register);

        initView();

    }

    private void initView() {
        mUserName = (EditText) findViewById(R.id.et_user_number);
        mPassword = (EditText) findViewById(R.id.et_password);
        mNumber = (EditText) findViewById(R.id.et_number);
        mRegister = (Button) findViewById(R.id.btn_register);

        mRegister.setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.equals(mRegister)) {
            mUser = mUserName.getText().toString().trim();
            mPass = mPassword.getText().toString().trim();
            mNum = mNumber.getText().toString().trim();

            if (mUser.isEmpty()) {
                Toast.makeText(Register.this, getResources().getString(R.string.register_name_illegal), Toast.LENGTH_SHORT).show();
            } else if (mPass.isEmpty()) {
                Toast.makeText(Register.this, getResources().getString(R.string.register_password_illegal_n), Toast.LENGTH_SHORT).show();
            } else if (mNum.length() != 11) {
                Toast.makeText(Register.this, getResources().getString(R.string.register_name_illegal), Toast.LENGTH_SHORT).show();
            } else {
                progressDialog = CustomProgressDialog.createDialog(Register.this);
                progressDialog.show();
                AVQuery<AVObject> query = new AVQuery<>("UserInfo");
                query.whereEqualTo("userName", mUser);
                query.countInBackground(new CountCallback() {
                    @Override
                    public void done(int count, AVException e) {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        if (e == null) {
                            if (count == 0) {
                                AVObject userInfo = new AVObject("UserInfo");
                                userInfo.put("userName", mUser);
                                userInfo.put("password", mPass);
                                userInfo.put("email", mNum);

                                userInfo.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(AVException e) {
                                        if (e != null) {
                                            Toast.makeText(Register.this, getResources().getString(R.string.register_fail), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(Register.this, getResources().getString(R.string.register_success), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                mPassword.setText("");
                                mUserName.setText("");
                                mNumber.setText("");

                                finish();
                            } else {
                                Toast.makeText(Register.this, getResources().getString(R.string.register_name_illegal_same), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(Register.this, getResources().getString(R.string.wrong_network), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

        if (view.getId() == R.id.back) {
            finish();
        }

    }
}
