package com.gaosir.www.healthassisant.secretary;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.AlarmDBManager;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.MedicalRemind;
import com.gaosir.www.healthassisant.data_format_tools.SecretaryDateTimeFormat;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-5-6
 */

public class AddMedicineAlarm extends Activity {

    private Calendar mCalendar = Calendar.getInstance();
    private Calendar calendarAlarm = Calendar.getInstance();
    private final static int REMEDIND_TYPE_ID = 0;

    private MedicalRemind medicalRemind = null;

    // private String medical_name = null;
    private int displayYear_1 = 0;
    private int displayMonth_1 = 0;
    private int displayDay_1 = 0;
    private int displayYear_2 = 0;
    private int displayMonth_2 = 0;
    private int displayDay_2 = 0;
    private int displayHour_1 = 0;
    private int displayMinute_1 = 0;
    private int displayHour_2 = 0;
    private int displayMinute_2 = 0;
    private int displayHour_3 = 0;
    private int displayMinute_3 = 0;
    private String tDate = "";

    private String _idString;
    private String alarmID_1;
    private String alarmID_2;
    private String alarmID_3;
    private int currentAlarmID = 0;
    private String alarmTitleString = null;
    private String transferType;

    private String advance;
    private AlarmDBManager mAlarmDBManager = null;
    private AlarmManager alarmManager = null;
    private EditText medicalName;
    private EditText dosageEditText;
    private EditText eventRemarks;
    private TextView start_date;
    private TextView end_date;

    private Boolean addState = false;
    private Boolean addTstate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.adddrugalarm);
        initview();
    }

    private void initview() {
        final ImageView back = (ImageView) findViewById(R.id.adddrug_back);
        TextView addcomplete = (TextView) findViewById(R.id.addcomplete);

        medicalName = (EditText) findViewById(R.id.alarm_drugname);
        dosageEditText = (EditText) findViewById(R.id.et_dosage);

        start_date = (TextView) findViewById(R.id.from_dateandweek);
        ImageView calendar_from = (ImageView) findViewById(R.id.calendar_from);

        end_date = (TextView) findViewById(R.id.to_dateandweek);
        ImageView calendar_to = (ImageView) findViewById(R.id.calendar_to);

        final TextView mortime = (TextView) findViewById(R.id.mortime);
        final CheckBox morcheck = (CheckBox) findViewById(R.id.morarrow);

        final TextView noontime = (TextView) findViewById(R.id.noontime);
        final CheckBox nooncheck = (CheckBox) findViewById(R.id.noonarrow);

        final TextView nighttime = (TextView) findViewById(R.id.nighttime);
        final CheckBox nightcheck = (CheckBox) findViewById(R.id.nightnarrow);

        Spinner repeatspinner = (Spinner) findViewById(R.id.repeatspinner);
        Spinner aheadspinner = (Spinner) findViewById(R.id.aheadspinner);
        eventRemarks = (EditText) findViewById(R.id.remark);

        medicalRemind = new MedicalRemind();

        mAlarmDBManager = new AlarmDBManager(getApplicationContext());
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmTitleString = getString(R.string.use_medicine_alarm);
        currentAlarmID = (int) (System.currentTimeMillis() / 60 / 1000);

        mCalendar.setTimeInMillis(System.currentTimeMillis());
        calendarAlarm.setTimeInMillis(System.currentTimeMillis());
        medicalRemind.setUser_name("user");

        Intent it = getIntent();
        transferType = it.getStringExtra("type");

        if (transferType.equals("add")) {

            medicalRemind.setEnd_date("0");
            medicalRemind.setAlarmID_1("0");
            medicalRemind.setAlarmID_2("0");
            medicalRemind.setAlarmID_3("0");
            medicalRemind.setRepeat_type("1");
            medicalRemind.setRemind_morning("8:00");
            medicalRemind.setRemind_noon("12:00");
            medicalRemind.setRemind_evening("18:00");

            displayYear_1 = mCalendar.get(Calendar.YEAR);
            displayMonth_1 = mCalendar.get(Calendar.MONTH);
            displayDay_1 = mCalendar.get(Calendar.DAY_OF_MONTH);
            displayYear_2 = mCalendar.get(Calendar.YEAR);
            displayMonth_2 = mCalendar.get(Calendar.MONTH);
            displayDay_2 = mCalendar.get(Calendar.DAY_OF_MONTH);

            displayHour_1 = 8;
            displayMinute_1 = 0;
            displayHour_2 = 12;
            displayMinute_2 = 0;
            displayHour_3 = 18;
            displayMinute_3 = 0;

            displayMonth_1 = displayMonth_1 + 1;

            start_date.setText(displayYear_1 + "-" + displayMonth_1 + "-"
                    + displayDay_1);
            medicalRemind.setStart_date(start_date.getText().toString().trim());

            displayMonth_2 = displayMonth_2 + 1;
            end_date.setText(displayYear_2 + "-" + displayMonth_2 + "-"
                    + displayDay_2);
            medicalRemind.setEnd_date(end_date.getText().toString().trim());

            displayMonth_1 = displayMonth_1 - 1;
            displayMonth_2 = displayMonth_2 - 1;

        } else {

            Log.i("Gao", "edit condition");
            _idString = it.getStringExtra("_id");
            alarmID_1 = it.getStringExtra("alarmID_1");
            alarmID_2 = it.getStringExtra("alarmID_2");
            alarmID_3 = it.getStringExtra("alarmID_3");
            medicalName.setText(it.getStringExtra("medical_name"));
            dosageEditText.setText(it.getStringExtra("dosage"));
            eventRemarks.setText(it.getStringExtra("remark"));

            start_date.setText(it.getStringExtra("start_date"));
            end_date.setText(it.getStringExtra("end_date"));
            displayYear_1 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("start_date"))[0];
            displayMonth_1 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("start_date"))[1];
            displayDay_1 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("start_date"))[2];

            displayYear_2 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("end_date"))[0];
            displayMonth_2 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("end_date"))[1];
            displayDay_2 = SecretaryDateTimeFormat.getSeparatedDate(it
                    .getStringExtra("end_date"))[2];

            mortime.setText(it.getStringExtra("remind_morning"));
            noontime.setText(it.getStringExtra("remind_noon"));
            nighttime.setText(it.getStringExtra("remind_evening"));
            medicalRemind
                    .setRemind_morning(it.getStringExtra("remind_morning"));
            medicalRemind.setRemind_noon(it.getStringExtra("remind_noon"));
            medicalRemind
                    .setRemind_evening(it.getStringExtra("remind_evening"));

            int repeat_type = 0;
            repeat_type = Integer.valueOf(it.getStringExtra("repeat_type"));
            if (repeat_type == 7) {
                repeat_type = 3;
            }
            repeatspinner.setSelection(repeat_type, true);
            medicalRemind.setRepeat_type(it.getStringExtra("repeat_type"));
            if (!(it.getStringExtra("advance") == null
                    || it.getStringExtra("advance").equals("") || it
                    .getStringExtra("advance").equals("null"))) {

                advance = it.getStringExtra("advance");
            } else {
                advance = "0";
            }
            medicalRemind.setAdvance(advance);
            aheadspinner.setSelection(
                    Integer.valueOf(it.getStringExtra("advance")), true);
            addTstate = true;

            // setAlarmTime(Integer.valueOf(timeStrings[0]),

            if (medicalRemind.getRemind_morning().equals("")) {
                mortime.setTextColor(getResources().getColor(
                        R.color.forbid_color));
                mortime.setText("8:00");
                morcheck.setChecked(false);
                displayHour_1 = 8;
                displayMinute_1 = 0;

            } else {
                morcheck.setChecked(true);
                mortime.setTextColor(getResources().getColor(
                        R.color.choose_time));
                String[] timeStrings = it.getStringExtra("remind_morning")
                        .split(":");
                displayHour_1 = Integer.valueOf(timeStrings[0]);
                displayMinute_1 = Integer.valueOf(timeStrings[1]);
            }

            if (medicalRemind.getRemind_noon().equals("")) {
                noontime.setTextColor(getResources().getColor(
                        R.color.forbid_color));
                noontime.setText("12:00");
                nooncheck.setChecked(false);

                displayHour_2 = 12;
                displayMinute_2 = 0;

            } else {
                noontime.setTextColor(getResources().getColor(
                        R.color.choose_time));
                nooncheck.setChecked(true);
                String[] timeStrings = it.getStringExtra("remind_noon").split(
                        ":");
                displayHour_2 = Integer.valueOf(timeStrings[0]);
                displayMinute_2 = Integer.valueOf(timeStrings[1]);
            }

            if (medicalRemind.getRemind_evening().equals("")) {

                nighttime.setTextColor(getResources().getColor(
                        R.color.forbid_color));
                nighttime.setText("18:00");
                nightcheck.setChecked(false);

                displayHour_3 = 18;
                displayMinute_3 = 0;

            } else {
                nighttime.setTextColor(getResources().getColor(
                        R.color.choose_time));
                nightcheck.setChecked(true);
                String[] timeStrings = it.getStringExtra("remind_evening")
                        .split(":");
                displayHour_3 = Integer.valueOf(timeStrings[0]);
                displayMinute_3 = Integer.valueOf(timeStrings[1]);
            }
        }

        OnClickListener ls = new OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.adddrug_back:
                        finish();
                        break;
                    case R.id.addcomplete:
                        String tMedicalNameString = medicalName.getText()
                                .toString().trim();
                        String tDosageString = dosageEditText.getText().toString()
                                .trim();

                        if (morcheck.isChecked() || nooncheck.isChecked()
                                || nightcheck.isChecked()) {
                            addTstate = true;
                        }

                        if (tMedicalNameString.equals("")
                                || tMedicalNameString == null) {
                            addState = false;
                        } else {
                            addState = true;
                        }

                        addState = addState && addTstate;

                        Log.i("Gao", "" + addState);

                        if (addState) {

                            medicalRemind.setMedical_name(tMedicalNameString);
                            medicalRemind.setDosage(tDosageString);
                            medicalRemind.setRemarks(eventRemarks.getText()
                                    .toString().trim());
                            medicalRemind.set_id(mAlarmDBManager.getMaxId() + 1);
                            // medicalRemind.alarmID = ""+maxAlarmID;

                            if (transferType.equals("edit")) {
                                String[] alarmIDStrings = {alarmID_1, alarmID_2,
                                        alarmID_3};
                                for (String string : alarmIDStrings) {
                                    if (!(string.equals("") || string == null)) {
                                        int cancelID = 0;
                                        cancelID = Integer.valueOf(string);
                                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                        alarmManager
                                                .cancel(PendingIntent
                                                        .getBroadcast(
                                                                getApplicationContext(),
                                                                cancelID,
                                                                new Intent(
                                                                        getApplicationContext(),
                                                                        AlarmReceiver.class),
                                                                0));
                                    }
                                }
                                // addToAlarm();
                                if (morcheck.isChecked()) {

                                    if (medicalRemind.getRemind_morning() == null
                                            || medicalRemind.getRemind_morning()
                                            .equals("")) {
                                        medicalRemind.setRemind_morning("8:00");
                                    }
                                    String[] tStrings = medicalRemind.remind_morning
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "mor_hour:" + tHour
                                            + " mor_minut: " + tMinute);
                                    int id = 1;
                                    Log.i("Gao", "tHour:" + tHour + "tMinute:"
                                            + tMinute + "id:" + id);
                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_morning = "";
                                }
                                if (nooncheck.isChecked()) {
                                    if (medicalRemind.getRemind_noon() == null
                                            || medicalRemind.getRemind_noon()
                                            .equals("")) {
                                        medicalRemind.setRemind_morning("12:00");
                                    }
                                    String[] tStrings = medicalRemind.remind_noon
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "noon_hour:" + tHour
                                            + " noon_minut: " + tMinute);
                                    int id = 2;
                                    Log.i("Gao", "tHour:" + tHour + "tMinute:"
                                            + tMinute + "id:" + id);

                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_noon = "";
                                }
                                if (nightcheck.isChecked()) {
                                    if (medicalRemind.getRemind_noon() == null
                                            || medicalRemind.getRemind_noon()
                                            .equals("")) {
                                        medicalRemind.setRemind_morning("18:00");
                                    }
                                    String[] tStrings = medicalRemind.remind_evening
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "evening_hour:" + tHour
                                            + " evening_minut: " + tMinute);
                                    int id = 3;
                                    Log.i("Gao", "tHour:" + tHour + "tMinute:"
                                            + tMinute + "id:" + id);
                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_evening = "";
                                }

                                Log.i("Gao", _idString);
                                System.out.println(medicalRemind);
                                Log.i("Gao", medicalRemind.remind_morning + "   "
                                        + medicalRemind.remind_noon + "   "
                                        + medicalRemind.remind_evening);
                                mAlarmDBManager.updateData(AddMedicineAlarm.this,
                                        medicalRemind, _idString);
                                finish();

                            } else {

                                // addToAlarm();
                                if (morcheck.isChecked()) {
                                    String[] tStrings = medicalRemind.remind_morning
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "mor_hour:" + tHour
                                            + " mor_minut: " + tMinute);
                                    int id = 1;
                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_morning = "";
                                }
                                if (nooncheck.isChecked()) {
                                    String[] tStrings = medicalRemind.remind_noon
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "noon_hour:" + tHour
                                            + " noon_minut: " + tMinute);
                                    int id = 2;
                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_noon = "";
                                }
                                if (nightcheck.isChecked()) {
                                    String[] tStrings = medicalRemind.remind_evening
                                            .split(":");
                                    int tHour = Integer.valueOf(tStrings[0]);
                                    int tMinute = Integer.valueOf(tStrings[1]);
                                    Log.i("Gao", "evening_hour:" + tHour
                                            + " evening_minut: " + tMinute);
                                    int id = 3;
                                    addToAlarm(tHour, tMinute, id);
                                } else {
                                    medicalRemind.remind_evening = "";
                                }

                                addToDB(medicalRemind);

                            }
                            finish();

                        } else {
                            Toast toast = Toast.makeText(AddMedicineAlarm.this,
                                    R.string.medical_reminder_complete,
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast.show();
                        }

                        break;

                    case R.id.calendar_from:
                        Log.i("Gao", "touch calendar");
                    case R.id.from_dateandweek:

                        DatePickerDialog startDay = new DatePickerDialog(
                                AddMedicineAlarm.this, startDatePickListener,
                                displayYear_1, displayMonth_1, displayDay_1);
                        startDay.show();
                        medicalRemind.start_date = tDate;
                        break;

                    case R.id.calendar_to:
                        Log.i("Gao", "touch calendar");
                    case R.id.to_dateandweek:

                        DatePickerDialog endDay = new DatePickerDialog(
                                AddMedicineAlarm.this, endDatePickListener,
                                displayYear_2, displayMonth_2, displayDay_2);
                        endDay.show();
                        break;

                    case R.id.mortime:
                        if (morcheck.isChecked()) {
                            new TimePickerDialog(v.getContext(),
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view,
                                                              int hourOfDay, int minute) {

                                            displayHour_1 = hourOfDay;
                                            displayMinute_1 = minute;

                                            if (minute < 10) {
                                                medicalRemind.remind_morning = ""
                                                        + hourOfDay + ":0" + minute;
                                            } else {
                                                medicalRemind.remind_morning = ""
                                                        + hourOfDay + ":" + minute;
                                            }

                                            mortime.setText(medicalRemind.remind_morning);
                                            Log.i("Gao", "moring"
                                                    + medicalRemind.remind_morning);
                                        }
                                    }, displayHour_1, displayMinute_1, true).show();
                        }
                        break;

                    case R.id.noontime:
                        if (nooncheck.isChecked()) {
                            new TimePickerDialog(v.getContext(),
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view,
                                                              int hourOfDay, int minute) {

                                            displayHour_2 = hourOfDay;
                                            displayMinute_2 = minute;

                                            if (minute < 10) {

                                                medicalRemind.remind_noon = ""
                                                        + hourOfDay + ":0" + minute;
                                            } else {

                                                medicalRemind.remind_noon = ""
                                                        + hourOfDay + ":" + minute;
                                            }
                                            noontime.setText(medicalRemind.remind_noon);
                                            Log.i("Gao", "noon: "
                                                    + medicalRemind.remind_noon);

                                        }
                                    }, displayHour_2, displayMinute_2, true).show();
                        }
                        break;

                    case R.id.nighttime:
                        if (nightcheck.isChecked()) {
                            new TimePickerDialog(v.getContext(),
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view,
                                                              int hourOfDay, int minute) {

                                            displayHour_3 = hourOfDay;
                                            displayMinute_3 = minute;
                                            // addTstate = true;

                                            if (minute < 10) {
                                                medicalRemind.remind_evening = ""
                                                        + hourOfDay + ":0" + minute;

                                            } else {
                                                medicalRemind.remind_evening = ""
                                                        + hourOfDay + ":" + minute;

                                            }
                                            nighttime
                                                    .setText(medicalRemind.remind_evening);
                                            Log.i("Gao", "evening: "
                                                    + medicalRemind.remind_evening);

                                        }
                                    }, displayHour_3, displayMinute_3, true).show();
                        }
                        break;
                    case R.id.morarrow:
                        if (morcheck.isChecked()) {
                            mortime.setTextColor(getResources().getColor(
                                    R.color.choose_time));

                        } else {
                            mortime.setTextColor(getResources().getColor(
                                    R.color.forbid_color));
                        }
                        break;
                    case R.id.noonarrow:
                        if (nooncheck.isChecked()) {
                            noontime.setTextColor(getResources().getColor(
                                    R.color.choose_time));
                        } else {
                            noontime.setTextColor(getResources().getColor(
                                    R.color.forbid_color));
                        }
                        break;
                    case R.id.nightnarrow:
                        if (nightcheck.isChecked()) {
                            nighttime.setTextColor(getResources().getColor(
                                    R.color.choose_time));
                        } else {
                            nighttime.setTextColor(getResources().getColor(
                                    R.color.forbid_color));
                        }
                        break;

                }
            }

        };

        start_date.setOnClickListener(ls);
        calendar_from.setOnClickListener(ls);
        end_date.setOnClickListener(ls);
        calendar_to.setOnClickListener(ls);

        mortime.setOnClickListener(ls);
        morcheck.setOnClickListener(ls);
        noontime.setOnClickListener(ls);
        nooncheck.setOnClickListener(ls);
        nighttime.setOnClickListener(ls);
        nightcheck.setOnClickListener(ls);

        addcomplete.setOnClickListener(ls);
        back.setOnClickListener(ls);
        repeatspinner.setOnItemSelectedListener(repeateItemSelectedListener);
        aheadspinner.setOnItemSelectedListener(advanceItemSelectedListener);

    }

    private Spinner.OnItemSelectedListener repeateItemSelectedListener = new Spinner.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            switch (arg2) {
                case 0:
                    medicalRemind.repeat_type = "" + 1;
                    break;
                case 1:
                    medicalRemind.repeat_type = "" + 2;
                    break;
                case 2:
                    medicalRemind.repeat_type = "" + 3;
                    break;
                case 3:
                    medicalRemind.repeat_type = "" + 7;
                    break;
                case 4:
                    medicalRemind.repeat_type = "" + 0;
                    break;

                default:
                    medicalRemind.repeat_type = "" + 1;
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            medicalRemind.repeat_type = "" + 1;
        }

    };

    private Spinner.OnItemSelectedListener advanceItemSelectedListener = new Spinner.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
            switch (arg2) {
                case 0:
                    advance = "0";
                    medicalRemind.setAdvance("0");
                    break;
                case 1:
                    advance = "10";
                    medicalRemind.setAdvance("1");
                    break;
                case 2:
                    medicalRemind.setAdvance("2");
                    advance = "15";
                    break;
                case 3:
                    medicalRemind.setAdvance("3");
                    advance = "20";
                    break;
                case 4:
                    medicalRemind.setAdvance("4");
                    advance = "30";
                    break;
                case 5:
                    medicalRemind.setAdvance("5");
                    advance = "60";
                    break;
                case 6:
                    medicalRemind.setAdvance("6");
                    advance = "1440";
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            if (transferType.equals("add")) {

                advance = "0";
                medicalRemind.setAdvance("0");

            }
        }

    };

    private DatePickerDialog.OnDateSetListener startDatePickListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            calendarAlarm.set(Calendar.YEAR, year);
            calendarAlarm.set(Calendar.MONTH, monthOfYear);
            calendarAlarm.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            displayYear_1 = year;
            displayMonth_1 = monthOfYear;
            displayDay_1 = dayOfMonth;
            tDate = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            Log.i("Gao", "from_date" + tDate);
            medicalRemind.start_date = tDate;
            start_date.setText(tDate);
            System.out.println(tDate);
        }

    };
    private DatePickerDialog.OnDateSetListener endDatePickListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            displayYear_2 = year;
            displayMonth_2 = monthOfYear;
            displayDay_2 = dayOfMonth;

            tDate = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            Log.i("Gao", "to_date" + tDate);
            medicalRemind.end_date = tDate;
            System.out.println(tDate);
            end_date.setText(tDate);
        }

    };

    private void addToDB(MedicalRemind medicalRemind) {

        mAlarmDBManager.add(this, medicalRemind);
        ArrayList<MedicalRemind> testList = new ArrayList<MedicalRemind>();
        testList = mAlarmDBManager.query();
        System.out.println(testList);

    }

    private void addToAlarm(int hour, int minute, int id) {
        Log.i("Gao", "addToAlarm start");

        calendarAlarm.set(Calendar.HOUR_OF_DAY, hour);
        calendarAlarm.set(Calendar.MINUTE, minute);
        calendarAlarm.set(Calendar.SECOND, 0);
        calendarAlarm.set(Calendar.MILLISECOND, 0);

        int alarmID = (int) (calendarAlarm.getTimeInMillis() / 1000 / 60);
        Log.i("Gao", "alarmID: " + alarmID);
        switch (id) {
            case 1:
                medicalRemind.alarmID_1 = "" + alarmID;
                break;
            case 2:
                medicalRemind.alarmID_2 = "" + alarmID;
                break;
            case 3:
                medicalRemind.alarmID_3 = "" + alarmID;
                break;

            default:
                break;
        }

        String medical_nameForAlarm = medicalRemind.medical_name + " "
                + mAlarmDBManager.alarmIdStrings(this, "" + alarmID);
        Log.i("Gao", medical_nameForAlarm);

        Intent intent = new Intent(AddMedicineAlarm.this, AlarmReceiver.class);
        intent.putExtra("alarmTitle", alarmTitleString);
        intent.putExtra("alarmText", getString(R.string.medical_alarm_text)
                + " " + medical_nameForAlarm + " "
                + getString(R.string.alarm_text_tail));
        intent.putExtra("alarmID", alarmID);
        intent.putExtra("end_day", medicalRemind.end_date);

        PendingIntent pIntent = PendingIntent.getBroadcast(
                getApplicationContext(), alarmID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        long triggerTime = calendarAlarm.getTimeInMillis()
                - (Long.valueOf(advance) * 60 * 1000);
        if (calendarAlarm.getTimeInMillis() > System.currentTimeMillis()) {
            Log.i("Gao", ">>system:" + calendarAlarm.getTimeInMillis());
            Log.i("Gao", ">>system:" + System.currentTimeMillis());

            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (medicalRemind.repeat_type.equals("0")) {

                alarmManager.set(AlarmManager.RTC_WAKEUP, triggerTime, pIntent);

            }
        } else {
            triggerTime += 24 * 3600000;
        }
        if (!medicalRemind.repeat_type.equals("0")) {
            long repeatTime = Long.valueOf(medicalRemind.repeat_type) * 24 * 3600000;
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerTime,
                    repeatTime, pIntent);

        }
        Log.i("Alarm", "setAlarm complete");
    }

    @SuppressWarnings("unused")
    private String getweek(int week) {
        String week_array[] = getResources().getStringArray(R.array.week);
        String weekday = "";
        switch (week) {
            case 1:
                weekday = week_array[0];
                break;
            case 2:
                weekday = week_array[1];
                break;
            case 3:
                weekday = week_array[2];
                break;
            case 4:
                weekday = week_array[3];
                break;
            case 5:
                weekday = week_array[4];
                break;
            case 6:
                weekday = week_array[5];
                break;
            case 7:
                weekday = week_array[6];
                break;

        }

        return weekday;
    }

}
