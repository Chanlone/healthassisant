package com.gaosir.www.healthassisant.data_format_tools;

import java.util.Calendar;

public class SecretaryDateTimeFormat {

    public static int[] getSeparatedDate(String start_date) {
        String[] dateStrings = start_date.split("-");
        int[] separatedDate = {0, 0, 0};
        separatedDate[0] = Integer.valueOf(dateStrings[0]);
        separatedDate[1] = Integer.valueOf(dateStrings[1]) - 1;
        separatedDate[2] = Integer.valueOf(dateStrings[2]);
        return separatedDate;
    }

    public static int getDateInt(String date) {
        String dateFormat = date.replace("-", "");
        return Integer.valueOf(dateFormat);
    }

    public static int[] getSeparatedTime(String start_time) {
        String[] timeStrings = start_time.split(":");
        int[] separatedTime = {0, 0};
        separatedTime[0] = Integer.valueOf(timeStrings[0]);
        separatedTime[1] = Integer.valueOf(timeStrings[1]);
        return separatedTime;

    }

    public static String getDisplayDate(Calendar calendar) {
        int displayYear = calendar.get(Calendar.YEAR);
        int displayMonth = calendar.get(Calendar.MONTH);
        int displayDay = calendar.get(Calendar.DAY_OF_MONTH);
        displayMonth = displayMonth + 1;

        return displayYear + "-" + displayMonth + "-" + displayDay;
    }

    public static String getStoreDate(Calendar calendar) {
        int displayYear = calendar.get(Calendar.YEAR) - 2000;
        int displayMonth = calendar.get(Calendar.MONTH) + 1;
        int displayDay = calendar.get(Calendar.DAY_OF_MONTH);
        String month;
        if (displayMonth < 10) {
            month = "0" + displayMonth;
        } else {
            month = "" + displayMonth;
        }
        String day = "";
        if (displayDay < 10) {
            day = "0" + displayDay;
        } else {
            day = "" + displayDay;
        }
        return "" + displayYear + month + day;
    }

    public static String getDisplayTime(Calendar calendar) {
        int displayHour = calendar.get(Calendar.HOUR_OF_DAY);
        int displayMinute = calendar.get(Calendar.MINUTE);
        String timeString;
        if (displayMinute < 10) {
            timeString = displayHour + ":0" + displayMinute;
        } else {
            timeString = displayHour + ":" + displayMinute;
        }

        return timeString;
    }

}
