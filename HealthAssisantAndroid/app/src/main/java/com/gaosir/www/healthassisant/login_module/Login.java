package com.gaosir.www.healthassisant.login_module;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.TabHostOld;
import com.gaosir.www.healthassisant.view_templet.CustomProgressDialog;

/**
 * @author Chanlone Gogh
 * @version 1.1.0 2016/8/9.
 */
public class Login extends Activity implements View.OnClickListener {
    private EditText mUsername;
    private EditText mPassword;

    private String mName = null;

    private static final String mTAG = "Login";

    CustomProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_login);

        mUsername = (EditText) findViewById(R.id.phonenumber);
        mPassword = (EditText) findViewById(R.id.passwd);

        SharedPreferences user_info = getSharedPreferences("user_info", 0);
        if (!user_info.getString("name", "").equals("guest_user")) {
            mUsername.setText(user_info.getString("name", ""));
        }

        findViewById(R.id.patient_login_btn).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.tv_forget_password).setOnClickListener(this);
        findViewById(R.id.tv_register).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.patient_login_btn:
                Log.i(mTAG, "" + view.getId());
                mName = mUsername.getText().toString().trim();
                if (mName.isEmpty()) {
                    Toast.makeText(Login.this, getResources().getString(R.string.register_name_illegal), Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Login.this, getResources().getString(R.string.register_password_illegal_n), Toast.LENGTH_SHORT).show();
                } else {
                    progressDialog = CustomProgressDialog
                            .createDialog(Login.this);
                    progressDialog.show();

                    AVQuery<AVObject> query = new AVQuery<>("UserInfo");
                    query.whereEqualTo("userName", mName);
                    Log.i(mTAG, "press");
                    query.getFirstInBackground(new GetCallback<AVObject>() {
                        @Override
                        public void done(AVObject avObject, AVException e) {
                            if (e == null) {
                                if (progressDialog != null)
                                    progressDialog.dismiss();
                                String servicePassword = avObject.getString("password");
                                if (servicePassword.isEmpty()) {
                                    Toast.makeText(Login.this, getResources().getString(R.string.register_name_illegal_none), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (mPassword.getText().toString().trim().equals(servicePassword)) {
                                    Log.i(mTAG, "login success");
                                    SharedPreferences user_info = getSharedPreferences("user_info", 0);
                                    SharedPreferences.Editor editor = user_info.edit();
                                    editor.putString("name", mName);
                                    editor.apply();
                                    Intent intent1 = new Intent();
                                    intent1.setClass(Login.this, TabHostOld.class);

                                    startActivity(intent1);
                                } else {

                                    Toast.makeText(Login.this, getResources().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                    Toast.makeText(Login.this, getResources().getString(R.string.wrong_network), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });

                }
                break;
            case R.id.tv_register:
                Log.i(mTAG, "" + view.getId());
                intent.setClass(this, Register.class);
                startActivity(intent);
                break;
            case R.id.tv_forget_password:
                Log.i(mTAG, "" + view.getId());
                intent.setClass(this, ForgetPassword.class);
                startActivity(intent);

                break;
            case R.id.back:
                finish();
                break;
        }

    }
}
