package com.gaosir.www.healthassisant.secretary;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.data_format_tools.SecretaryDateTimeFormat;

import java.util.Calendar;

//import android.speech.tts.Voice;

public class AlarmDialogSet extends Activity {

	private String alarmTitle = null;
	private String alarmText = null;
	private String end_day = null;

	private Calendar calendar = Calendar.getInstance();
	private int alarmID = 0;
	AudioManager audioManager = null;
	MediaPlayer mediaPlayer = null;

	// Voice voice = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Make sure alarmTitle and alarmText is not null.
		alarmTitle = getString(R.string.null_alarm_title);
		alarmText = getString(R.string.null_alarm_text);

		// Get data from "AlarmReceiver".
		Intent it = getIntent();
		alarmTitle = it.getStringExtra("alarmTitle");
		alarmText = it.getStringExtra("alarmText");
		end_day = it.getStringExtra("end_day");
		alarmID = it.getIntExtra("alarmID", 0);

		// Judge whether it is the end day of repeat_type reminder.
		if (!end_day.equals("0")) {

			String end_day = it.getStringExtra("end_day");
			calendar.set(SecretaryDateTimeFormat.getSeparatedDate(end_day)[0],
					SecretaryDateTimeFormat.getSeparatedDate(end_day)[1],
					SecretaryDateTimeFormat.getSeparatedDate(end_day)[2], 0, 0);

			if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
				AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
				alarmManager.cancel(PendingIntent.getBroadcast(
						getApplicationContext(), alarmID, new Intent(
								getApplicationContext(), AlarmReceiver.class),
						0));
			}

		}

		// set alarm ringing if system not in RINGER_MODE_SILENT.
		audioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
		mediaPlayer = MediaPlayer.create(AlarmDialogSet.this, RingtoneManager
				.getActualDefaultRingtoneUri(AlarmDialogSet.this,
						RingtoneManager.TYPE_ALARM));
		switch (audioManager.getRingerMode()) {
		case AudioManager.RINGER_MODE_SILENT:

			break;
		case AudioManager.RINGER_MODE_NORMAL:
			mediaPlayer.setVolume(
					audioManager.getStreamVolume(AudioManager.STREAM_ALARM),
					audioManager.getStreamVolume(AudioManager.STREAM_ALARM));
			mediaPlayer.setLooping(true);
			mediaPlayer.start();
			break;

		case AudioManager.RINGER_MODE_VIBRATE:
			// TODO 手机震动模式下的提醒方式
			break;

		default:
			// TODO 默认提醒方式
			break;
		}
		Log.i("Gao", "AlertDialog build start");

		// set dialog style and show it.
		new AlertDialog.Builder(AlarmDialogSet.this)
				.setIcon(R.drawable.remind_icon).setTitle(alarmTitle)
				.setMessage(alarmText)
				.setPositiveButton(R.string.i_know_it, new OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						mediaPlayer.stop();

						AlarmDialogSet.this.finish();
					}
				}).create().show();

		Log.i("Gao", "AlerDialog build end");
	}

}
