package com.gaosir.www.healthassisant.risk_inspect;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gaosir.www.healthassisant.R;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-6-16
 */
public class DiseaseRisk extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private ListView mListView;
    private ImageView mBack;
    private String[] mList_content;
    private String[] mAdapt_crowd;
    private String[] mReportContentOne;
    private MAdapter mAdapter;
    public static final String TAG = "DisR";
    private int[] ImageId = {R.drawable.r_ten, R.drawable.r_eleven, R.drawable.r_ninth, R.drawable.r_fiveteen};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.disease_risk);
        initView();
    }

    private void initView() {
        mBack = (ImageView) findViewById(R.id.btn_back_top);
        mBack.setOnClickListener(this);
        mListView = (ListView) findViewById(R.id.risk_list);
        mList_content = getResources().getStringArray(R.array.risk_assessment);
        mAdapt_crowd = getResources().getStringArray(R.array.adapt_crowd);
        mReportContentOne = getResources().getStringArray(R.array.report_content);
        mAdapter = new MAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(mBack)) {
            mBack.setBackgroundColor(Color.argb(80, 255, 255, 255));
            finish();
        }

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent();
        switch (i) {
            case 0:
                intent.setClass(getApplicationContext(), TenYearCoronay.class);
                break;
            case 1:
                intent.setClass(getApplicationContext(), TenYearFC.class);
                break;
            case 2:
                intent.setClass(getApplicationContext(), AnxietyEvaluate.class);
                break;
            case 3:
                //TODO 抑郁的量表重写，Class建好了，没有写内容。
                intent.setClass(getApplicationContext(), AnxietyEvaluate.class);
                System.out.print("TODO");
                break;

        }
        intent.putExtra("title", mList_content[i]);
        intent.putExtra("adapt_crowd", mAdapt_crowd[i]);
        Log.i(TAG, mAdapt_crowd[i]);
        intent.putExtra("report_content_one", mReportContentOne[i]);
        startActivity(intent);
    }

    private class MAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        @Override
        public int getCount() {
            return mList_content.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.risk_list_item, null);

            ImageView mimage = (ImageView) convertView
                    .findViewById(R.id.myimageview);
            TextView mtextview = (TextView) convertView
                    .findViewById(R.id.mytextview);
            ImageView arrow = (ImageView) convertView.findViewById(R.id.arrow);
            mimage.setImageResource(ImageId[position]);
            mtextview.setText(mList_content[position]);
            arrow.setImageResource(R.drawable.arrow);

            return convertView;
        }
    }
}
