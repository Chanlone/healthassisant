package com.gaosir.www.healthassisant.login_module;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.gaosir.www.healthassisant.R;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2016/8/27.
 */
public class SlidingActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null){
            replaceTutorialFragment();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bRetry:
                replaceTutorialFragment();
                break;
        }

    }

    public void replaceTutorialFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new CustomPresentationPagerFragment());
        fragmentTransaction.commit();
    }
}
