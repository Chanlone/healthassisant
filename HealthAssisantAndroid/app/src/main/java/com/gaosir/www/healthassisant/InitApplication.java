package com.gaosir.www.healthassisant;

import android.app.Application;

import com.avos.avoscloud.AVOSCloud;

/**
 * @author Chanlone Gogh
 * @version 1.10 2016/8/11.
 */
public class InitApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // init parameters are this, AppID, AppKey
        AVOSCloud.initialize(this, "l733wr98ecnv3egllyqw8fw86twbdejk6302i6r3iwibiqi0","qc37x5x6tm9dz9it8n7varctbgg46x12296wjyzmj1wg3h91");
        // use Chinese service node
        AVOSCloud.useAVCloudCN();
    }
}
