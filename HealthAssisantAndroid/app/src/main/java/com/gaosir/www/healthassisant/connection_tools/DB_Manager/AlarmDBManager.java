package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class AlarmDBManager {
    private AlarmDBHelper helper;
    private static SQLiteDatabase db;

    public AlarmDBManager(Context context) {
        helper = new AlarmDBHelper(context);
        db = helper.getWritableDatabase();
    }

    public void add(Context context, MedicalRemind medicalRemind) {
        db.beginTransaction();
        try {
            db.execSQL(
                    "INSERT INTO medical_remind VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    new Object[]{medicalRemind._id, medicalRemind.user_name,
                            medicalRemind.medical_name, medicalRemind.dosage,
                            medicalRemind.start_date, medicalRemind.end_date,
                            medicalRemind.remind_morning,
                            medicalRemind.remind_noon,
                            medicalRemind.remind_evening,
                            medicalRemind.repeat_type, medicalRemind.advance,
                            medicalRemind.alarmID_1, medicalRemind.alarmID_2,
                            medicalRemind.alarmID_3, medicalRemind.remarks});
            db.setTransactionSuccessful();
        } finally {
            Log.i("Gao", "insert complete");
            db.endTransaction();
        }

    }

    public int getMaxId() {
        int id = 0;
        Cursor cursor = queryTheCursor();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToLast();
            id = cursor.getInt(cursor.getColumnIndex("_id"));
        }
        try {
            assert cursor != null;
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public void deleteData(Context context, String _idString) {
        db.delete("medical_remind", "_id = ?",
                new String[]{String.valueOf(_idString)});
    }

    public void updateData(Context context, MedicalRemind medicalRemind,
                           String _idString) {
        Log.i("Gao", medicalRemind.remind_morning + medicalRemind.remind_noon
                + medicalRemind.remind_evening);
        Log.i("Gao", "start update");

        String whereClause = "_id = ? and user_name = ?";
        String[] whereArgs = {_idString,
                "user"};
        System.out.println(String.valueOf(_idString));
        ContentValues contentValues = new ContentValues();
        contentValues.put("medical_name", medicalRemind.medical_name);
        contentValues.put("dosage", medicalRemind.dosage);
        contentValues.put("end_date", medicalRemind.end_date);
        contentValues.put("remind_morning", medicalRemind.remind_morning);
        contentValues.put("remind_noon", medicalRemind.remind_noon);
        contentValues.put("remarks", medicalRemind.remarks);
        contentValues.put("remind_evening", medicalRemind.remind_evening);
        contentValues.put("repeat_type", medicalRemind.repeat_type);
        contentValues.put("advance", medicalRemind.advance);
        contentValues.put("start_date", medicalRemind.start_date);
        contentValues.put("alarmID_1", medicalRemind.alarmID_1);
        contentValues.put("alarmID_2", medicalRemind.alarmID_2);
        contentValues.put("alarmID_3", medicalRemind.alarmID_3);
        db.update(AlarmDBHelper.TB_MEDICAL_REMINDS, contentValues, whereClause,
                whereArgs);

    }

    public MedicalRemind addToService() {

        Cursor cursor = queryTheCursor();
        cursor.moveToLast();
        MedicalRemind medicalRemind = new MedicalRemind();
        medicalRemind._id = cursor.getInt(cursor.getColumnIndex("_id"));
        medicalRemind.medical_name = cursor.getString(cursor
                .getColumnIndex("medical_name"));
        medicalRemind.dosage = cursor
                .getString(cursor.getColumnIndex("dosage"));
        medicalRemind.start_date = cursor.getString(cursor
                .getColumnIndex("start_date"));
        medicalRemind.end_date = cursor.getString(cursor
                .getColumnIndex("end_date"));
        medicalRemind.remarks = cursor.getString(cursor
                .getColumnIndex("remarks"));
        medicalRemind.remind_evening = cursor.getString(cursor
                .getColumnIndex("remind_evening"));
        medicalRemind.remind_noon = cursor.getString(cursor
                .getColumnIndex("remind_noon"));
        medicalRemind.remind_morning = cursor.getString(cursor
                .getColumnIndex("remind_morning"));
        medicalRemind.repeat_type = cursor.getString(cursor
                .getColumnIndex("repeat_type"));
        medicalRemind.advance = cursor.getString(cursor
                .getColumnIndex("advance"));
        medicalRemind.setAlarmID_1(cursor.getString(cursor
                .getColumnIndex("alarmID_1")));
        medicalRemind.setAlarmID_2(cursor.getString(cursor
                .getColumnIndex("alarmID_2")));
        medicalRemind.setAlarmID_3(cursor.getString(cursor
                .getColumnIndex("alarmID_3")));
        cursor.close();
        return medicalRemind;
    }

    public ArrayList<MedicalRemind> query() {
        ArrayList<MedicalRemind> medicalReminds = new ArrayList<MedicalRemind>();
        Cursor c = queryTheCursor();
        if (c != null && c.getCount() > 0) {

            while (c.moveToNext()) {
                MedicalRemind medicalRemind = new MedicalRemind();
                medicalRemind.set_id(c.getInt(c.getColumnIndex("_id")));
                medicalRemind.setAlarmID_1(c.getString(c
                        .getColumnIndex("alarmID_1")));
                medicalRemind.setAlarmID_2(c.getString(c
                        .getColumnIndex("alarmID_2")));
                medicalRemind.setAlarmID_3(c.getString(c
                        .getColumnIndex("alarmID_3")));
                medicalRemind.setMedical_name(c.getString(c
                        .getColumnIndex("medical_name")));
                medicalRemind
                        .setDosage(c.getString(c.getColumnIndex("dosage")));
                medicalRemind.setStart_date(c.getString(c
                        .getColumnIndex("start_date")));
                medicalRemind.setEnd_date(c.getString(c
                        .getColumnIndex("end_date")));
                medicalRemind.setRemind_morning(c.getString(c
                        .getColumnIndex("remind_morning")));
                medicalRemind.setRemind_noon(c.getString(c
                        .getColumnIndex("remind_noon")));
                medicalRemind.setRemind_evening(c.getString(c
                        .getColumnIndex("remind_evening")));
                medicalRemind.setRepeat_type(c.getString(c
                        .getColumnIndex("repeat_type")));
                medicalRemind.setAdvance(c.getString(c
                        .getColumnIndex("advance")));
                medicalRemind.setRemarks(c.getString(c
                        .getColumnIndex("remarks")));
                medicalReminds.add(medicalRemind);
                Log.i("Gao", "id is " + medicalRemind._id);

            }
        }
        c.close();
        return medicalReminds;

    }

    public String alarmIdStrings(Context context, String alarmID) {
        String medical_name = "";
        Cursor cursor = querySameTime(alarmID);
        Log.i("Gao", "same id:" + cursor.getCount());
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                medical_name = medical_name
                        + cursor.getString(cursor
                        .getColumnIndex("medical_name")) + " ";
            }
        }
        Log.i("Gao", medical_name);

        return medical_name;

    }

    public static Cursor querySameTime(String alarmIDstring) {
        Cursor cursor = db
                .rawQuery(
                        "SELECT * FROM medical_remind WHERE alarmID_1 = ? or alarmID_2 = ? or alarmID_3 = ?",
                        new String[]{alarmIDstring, alarmIDstring,
                                alarmIDstring});
        return cursor;

    }

    private static Cursor queryTheCursor() {
        Cursor c = db.rawQuery(
                "SELECT * FROM medical_remind WHERE user_name = ?",
                new String[]{"user"});

        return c;
    }

    public void closeDB() {
        db.close();
    }

}
