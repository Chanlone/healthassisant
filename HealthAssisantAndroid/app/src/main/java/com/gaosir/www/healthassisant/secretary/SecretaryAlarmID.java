package com.gaosir.www.healthassisant.secretary;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class SecretaryAlarmID {

	private final static int[] li_SecPosValue = { 1601, 1637, 1833, 2078, 2274,
			2302, 2433, 2594, 2787, 3106, 3212, 3472, 3635, 3722, 3730, 3858,
			4027, 4086, 4390, 4558, 4684, 4925, 5249, 5590 };
	private final static String[] lc_FirstLetter = { "a", "b", "c", "d", "e",
			"f", "g", "h", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
			"t", "w", "x", "y", "z" };

	/**
	 * 取得给定汉字串的首字母串,即声母串
	 * 
	 * @param str
	 * @return 声母
	 */
	public String getAllFirstLetter(String str) {
		if (str == null || str.trim().length() == 0) {
			return "";
		}

		String _str = "";
		for (int i = 0; i < str.length(); i++) {
			_str = _str + this.getFirstLetter(str.substring(i, i + 1));
		}

		return _str;
	}

	/**
	 * 取得给定汉字的首字母,即声
	 * 
	 * @param chinese
	 * @return 给定汉字的声
	 */
	public String getFirstLetter(String chinese) {
		if (chinese == null || chinese.trim().length() == 0) {
			return "";
		}
		chinese = this.conversionStr(chinese, "GB2312", "ISO8859-1");

		if (chinese.length() > 1) // 判断是不是汉�?
		{
			int li_SectorCode = (int) chinese.charAt(0); // 汉字区码
			int li_PositionCode = (int) chinese.charAt(1); // 汉字位码
			li_SectorCode = li_SectorCode - 160;
			li_PositionCode = li_PositionCode - 160;
			int li_SecPosCode = li_SectorCode * 100 + li_PositionCode; // 汉字区位�?
			if (li_SecPosCode > 1600 && li_SecPosCode < 5590) {
				for (int i = 0; i < 23; i++) {
					if (li_SecPosCode >= li_SecPosValue[i]
							&& li_SecPosCode < li_SecPosValue[i + 1]) {
						chinese = lc_FirstLetter[i];
						break;
					}
				}
			} else // 非汉字字�?,如图形符号或ASCII�?
			{
				chinese = this.conversionStr(chinese, "ISO8859-1", "GB2312");
				chinese = chinese.substring(0, 1);
			}
		}

		return chinese;
	}

	/**
	 * 字符串编码转
	 * 
	 * @param str
	 *            要转换编码的字符
	 * @param charsetName
	 *            原来的编
	 * @param toCharsetName
	 *            转换后的编码
	 * @return 经过编码转换后的字符
	 */
	private String conversionStr(String str, String charsetName,
			String toCharsetName) {
		try {
			str = new String(str.getBytes(charsetName), toCharsetName);
		} catch (UnsupportedEncodingException ex) {
			System.out.println("字符串编码转换异常：" + ex.getMessage());
		}
		return str;
	}

	public static int getAlarmID(String string, int type) {
		Random random = new Random();
		int id2 = random.nextInt(1000);
		SecretaryAlarmID abc = new SecretaryAlarmID();
		char[] letters = abc.getAllFirstLetter(string).toCharArray();
		int id1 = (letters[0] - 96) * 26 + (letters[letters.length - 1] - 96);
		int id3 = (int) (System.currentTimeMillis() % 1000);
		return id1 * 1000 * 1000 + id2 * 1000 + id3 + type;
	}

}
