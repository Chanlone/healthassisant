package com.gaosir.www.healthassisant.signs_data;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ItemDBManager;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ItemData;
import com.gaosir.www.healthassisant.data_format_tools.SecretaryDateTimeFormat;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-5-28
 */
public class SignsDataTab extends Activity implements View.OnClickListener {

    private ImageButton mAddItem;
    private LineChart mLineChart;
    private PieChart mPieChart;

    private TextView btnHeartRate;
    private TextView btnBloodIn;
    private TextView btnBloodOut;

    private LinearLayout mNoData;
    private LinearLayout mHasData;

    private Boolean mUploadTag = false;
    private Boolean mChartTag = false;

    private Calendar mCalendar = Calendar.getInstance();
    private Calendar tCanlendar = Calendar.getInstance();

    private ItemDBManager mItemDBManager;
    private ItemData mItemData;
    private List<AVObject> mList = null;

    public static final String TAG_HEART_RATE = "1";
    public static final String TAG_BLOOD_PRESSURE = "2";
    private static final int QB_DAY = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signs_data_tab);
        initView();
        initData();
        queryAll("heart_rate", 110, 150, 40);
    }

    private void initView() {
        mNoData = (LinearLayout) findViewById(R.id.ll_no_data);
        mHasData = (LinearLayout) findViewById(R.id.ll_has_data);

        btnHeartRate = (TextView) findViewById(R.id.tv_heart_rate);
        btnBloodOut = (TextView) findViewById(R.id.tv_blood_out);
        btnBloodIn = (TextView) findViewById(R.id.tv_blood_in);

        mAddItem = (ImageButton) findViewById(R.id.btn_add_item);
        mLineChart = (LineChart) findViewById(R.id.line_chart);
        mPieChart = (PieChart) findViewById(R.id.pie_chart);

        mAddItem.setOnClickListener(this);
        btnHeartRate.setOnClickListener(this);
        btnBloodIn.setOnClickListener(this);
        btnBloodOut.setOnClickListener(this);

    }

    private void initData() {
        // Query From cloud
        mItemDBManager = new ItemDBManager(SignsDataTab.this);
        mItemData = new ItemData();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        int today = Integer.valueOf(SecretaryDateTimeFormat.getStoreDate(mCalendar));

        SharedPreferences user_info = getSharedPreferences("user_info", 0);
        mItemData.user = user_info.getString("name", "user");

    }

    private void queryAll(final String item, final float limitLine, final float maxValue, final float minValue) {

        AVQuery<AVObject> query = new AVQuery<>("HeartBlood");
        query.whereEqualTo("name", mItemData.user);
        query.whereEqualTo("item", item);
        Log.i("Gao", mItemData.user);
        query.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    Log.i("Gao", "success");
                    mList = list;
                    if (!list.isEmpty()) {
                        mNoData.setVisibility(View.INVISIBLE);
                        mHasData.setVisibility(View.VISIBLE);
                        mHasData.postInvalidate();
                        LineData mLineData = createLineData(mList, QB_DAY, item);
                        showLineChart(mLineChart, mLineData, limitLine, maxValue, minValue);

                        PieData mPieData = createPieData(mList, item);
                        showPieChart(mPieChart, mPieData, item);

                    } else {
                        mNoData.setVisibility(View.VISIBLE);
                        mHasData.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

    }

    private void showLineChart(LineChart lineChart, LineData lineData, float limitLine, float maxValue, float minValue) {
        lineChart.notifyDataSetChanged();
        lineChart.removeAllViews();
        lineChart.invalidate();
        YAxis yAxis = lineChart.getAxisLeft();
        YAxis yAxisR = lineChart.getAxisRight();
        yAxisR.setEnabled(false);
        yAxis.removeAllLimitLines();
        yAxis.setAxisLineWidth(3f);
        yAxis.setAxisLineColor(getResources().getColor(R.color.holo_blue_dark));
        yAxis.setAxisMaxValue(maxValue);
        yAxis.setAxisMinValue(minValue);

        LimitLine ll = new LimitLine(limitLine, "警戒线");
        ll.setLineColor(getResources().getColor(R.color.holo_red_dark));
        ll.setLineWidth(3f);
        ll.setTextSize(14f);
        yAxis.addLimitLine(ll);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineWidth(3f);
        xAxis.setAxisLineColor(getResources().getColor(R.color.holo_blue_dark));

        lineChart.setDrawBorders(false);
        lineChart.setDescription(getResources().getString(R.string.evaluation));
        lineChart.setNoDataTextDescription("You need to provide data for the chart.");
        lineChart.setBackgroundColor(getResources().getColor(R.color.normal_bg_color));
        lineChart.setDrawGridBackground(false);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setData(lineData);
        Legend mLegend = lineChart.getLegend();
        mLegend.setForm(Legend.LegendForm.SQUARE);
        lineChart.animateY(1500);
    }

    private void showPieChart(PieChart pieChart, PieData pieData, String item) {
        pieChart.setHoleColorTransparent(true);

        pieChart.setHoleRadius(60f);  //半径
        pieChart.setTransparentCircleRadius(30f); // 半透明圈
        pieChart.setDrawCenterText(true);  //饼状图中间可以添加文字
        pieChart.setCenterTextSize(18f);
        pieChart.setHoleRadius(25f);  //实心圆
        pieChart.setBackgroundColor(getResources().getColor(R.color.normal_bg_color));

        item = get_item_name(item);
        pieChart.setDescription(item + "分布图");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setRotationAngle(90); // 初始旋转角度
        pieChart.setRotationEnabled(true); // 可以手动旋转

        pieChart.setUsePercentValues(true);  //显示成百分比
        pieChart.setCenterText(item);  //饼状图中间的文字

        //设置数据
        pieChart.setData(pieData);

        Legend mLegend = pieChart.getLegend();  //设置比例图
        mLegend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);  //最右边显示
//      mLegend.setForm(LegendForm.LINE);  //设置比例图的形状，默认是方形
        mLegend.setXEntrySpace(7f);
        mLegend.setYEntrySpace(5f);

        pieChart.animateXY(1500, 1500);
    }

    private LineData createLineData(List<AVObject> list, int show_type, String item) {
        ArrayList<String> interval = new ArrayList<>();
        ArrayList<Entry> coordinate = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            interval.add("" + i);
        }
        for (AVObject avObject : list) {
            int value = (int) avObject.get("value");
            coordinate.add(new Entry(value, list.indexOf(avObject)));
        }
        item = get_item_name(item);
        LineDataSet lineDataSet = new LineDataSet(coordinate, item);

        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(lineDataSet);

        return new LineData(interval, lineDataSets);

    }

    private ArrayList<String> get_item_separation(String item) {
        ArrayList<String> interval = new ArrayList<>();
        switch (item) {
            case "heart_rate":
                interval.add("心率60以下");
                interval.add("心率60-85");
                interval.add("心率85-110");
                interval.add("心率110以上");
                break;
            case "blood_pressure_in":
                interval.add("舒张压65以下");
                interval.add("舒张压65-85");
                interval.add("舒张压85-100");
                interval.add("舒张压100以上");
                break;
            case "blood_pressure":
                interval.add("收缩压90以下");
                interval.add("收缩压90-110");
                interval.add("收缩压110-140");
                interval.add("收缩压140以上");
                break;
        }
        return interval;
    }

    private String get_item_name(String item) {
        switch (item) {
            case "heart_rate":
                item = "心率";
                break;
            case "blood_pressure_in":
                item = "舒张压";
                break;
            case "blood_pressure":
                item = "收缩压";
                break;
        }
        return item;
    }

    private PieData createPieData(List<AVObject> list, String item) {
        ArrayList<String> xValues = get_item_separation(item);

        int total = list.size();
        float qu1 = 0;
        float qu2 = 0;
        float qu3 = 0;
        float qu4 = 0;

        switch (item) {
            case "heart_rate":
                for (AVObject avObject : list) {
                    int value = (int) avObject.get("value");
                    if (value < 60) {
                        qu1++;
                    } else if (value < 85) {
                        qu2++;
                    } else if (value < 110) {
                        qu3++;
                    } else {
                        qu4++;
                    }
                }
                break;
            case "blood_pressure_in":
                for (AVObject avObject : list) {
                    int value = (int) avObject.get("value");
                    if (value < 65) {
                        qu1++;
                    } else if (value < 85) {
                        qu2++;
                    } else if (value < 100) {
                        qu3++;
                    } else {
                        qu4++;
                    }
                }
                break;
            case "blood_pressure":
                for (AVObject avObject : list) {
                    int value = (int) avObject.get("value");
                    if (value < 90) {
                        qu1++;
                    } else if (value < 110) {
                        qu2++;
                    } else if (value < 140) {
                        qu3++;
                    } else {
                        qu4++;
                    }
                }
                break;
        }
        for (AVObject avObject : list) {
            int value = (int) avObject.get("value");
            if (value < 60) {
                qu1++;
            } else if (value < 85) {
                qu2++;
            } else if (value < 110) {
                qu3++;
            } else {
                qu4++;
            }
        }

        ArrayList<Entry> yValues = new ArrayList<>();

        yValues.add(new Entry(qu1 / total, 0));
        yValues.add(new Entry(qu2 / total, 1));
        yValues.add(new Entry(qu3 / total, 2));
        yValues.add(new Entry(qu4 / total, 3));

        PieDataSet pieDataSet = new PieDataSet(yValues, "");
        pieDataSet.setSliceSpace(0f);
        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(Color.rgb(135, 200, 250));
        colors.add(Color.rgb(240, 220, 130));
        colors.add(Color.rgb(180, 240, 60));
        colors.add(Color.rgb(255, 165, 80));

        pieDataSet.setColors(colors);
        pieDataSet.setValueTextColor(getResources().getColor(R.color.normal_bg_color));

        DisplayMetrics metrics = getResources().getDisplayMetrics();

        return new PieData(xValues, pieDataSet);
    }

    private void addDataToCloud(ItemData itemData) {

        AVObject heartRate = new AVObject("HeartBlood");

        heartRate.put("name", itemData.user);
        heartRate.put("item", itemData.item);
        heartRate.put("time", itemData.time);
        heartRate.put("date", Integer.valueOf(itemData.date));
        heartRate.put("value", Integer.valueOf(itemData.value));
        heartRate.put("itemTag", itemData.itemTag);

        heartRate.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    Toast.makeText(SignsDataTab.this, getResources().getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignsDataTab.this, getResources().getString(R.string.upload_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void alertDialog() {
        final Dialog dialog = new Dialog(SignsDataTab.this, R.style.dialog);
        dialog.setContentView(R.layout.dialog_heart_blood);
        final EditText ETHeartRate = (EditText) dialog.findViewById(R.id.et_heart_rate);
        final EditText ETBloodPressure = (EditText) dialog.findViewById(R.id.et_blood_pressure);
        final EditText ETBloodPressure_in = (EditText) dialog.findViewById(R.id.et_blood_pressure_in);
        Button upload = (Button) dialog.findViewById(R.id.btn_upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String heartRate = ETHeartRate.getText().toString().trim();
                String bloodPressure = ETBloodPressure.getText().toString().trim();
                String bloodPressureIn = ETBloodPressure_in.getText().toString().trim();
                mCalendar.setTimeInMillis(System.currentTimeMillis());
                mItemData.date = SecretaryDateTimeFormat.getStoreDate(mCalendar);
                mItemData.time = SecretaryDateTimeFormat.getDisplayTime(mCalendar);
                if (!heartRate.equals("")) {
                    mUploadTag = true;
                    mItemData.item = "heart_rate";
                    mItemData.itemTag = TAG_HEART_RATE;
                    mItemData.value = heartRate;
                    mItemDBManager.add(SignsDataTab.this, mItemData);
                    addDataToCloud(mItemData);

                }
                if (!bloodPressure.equals("")) {
                    mUploadTag = true;
                    mItemData.item = "blood_pressure";
                    mItemData.itemTag = TAG_BLOOD_PRESSURE;
                    mItemData.value = bloodPressure;
                    mItemDBManager.add(SignsDataTab.this, mItemData);
                    addDataToCloud(mItemData);
                }
                if (!bloodPressureIn.equals("")) {

                    mUploadTag = true;
                    mItemData.item = "blood_pressure_in";
                    mItemData.itemTag = TAG_BLOOD_PRESSURE;
                    mItemData.value = bloodPressure;
                    mItemDBManager.add(SignsDataTab.this, mItemData);
                    addDataToCloud(mItemData);

                }
                if (mUploadTag) {
                    dialog.dismiss();
                } else {
                    Toast.makeText(SignsDataTab.this, getResources().getString(R.string.upload_miss_data), Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_item:
                Log.i("Gao", "click:" + view.getId());
                alertDialog();
                break;
            case R.id.tv_heart_rate:
                // item, limit_line, max_value, min_value
                queryAll("heart_rate", 110, 150, 40);
                break;
            case R.id.tv_blood_in:
                queryAll("blood_pressure_in", 90, 110, 50);
                break;
            case R.id.tv_blood_out:
                queryAll("blood_pressure", 140, 190, 90);
                break;
        }

    }
}
