package com.gaosir.www.healthassisant.risk_inspect;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gaosir.www.healthassisant.R;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-6-16
 */
public class TenYearFC extends Activity implements View.OnClickListener {
    //TODO 巨烂又不能重用的代码
    private ImageView back;
    private Button submit;
    private TextView title;
    private TextView adaptcrowd;
    private String reportcontentone;
    private EditText age;
    private RadioButton radioman;
    private RadioButton radiowomen;
    private RadioButton radiopredown;
    private RadioButton radionopredown;
    private EditText sbp;
    private EditText weight;
    private EditText high;
    private EditText pr;
    private EditText xzzy;
    private EditText xlsj;
    private int[] man_age = {-3, -2, 0, 1, 3, 4, 6, 7};
    private int[] women_age = {1, 2, 3, 4, 5, 6, 7, 7};
    private int[] xzzy_age = {5, 4, 2, 1};
    private int[] xlsj_age = {10, 6, 2};
    private int[] pro = {2, 2, 3, 4, 6, 8, 12, 16, 22};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ten_year_fc);
        initview();
    }

    private void initview() {
        back = (ImageView) findViewById(R.id.back);
        submit = (Button) findViewById(R.id.submit);
        back.setOnClickListener(this);
        submit.setOnClickListener(this);

        title = (TextView) findViewById(R.id.title);
        adaptcrowd = (TextView) findViewById(R.id.adaptcrowd);

        age = (EditText) findViewById(R.id.age);
        sbp = (EditText) findViewById(R.id.sbp);
        weight = (EditText) findViewById(R.id.tz);
        high = (EditText) findViewById(R.id.sg);
        pr = (EditText) findViewById(R.id.pr);
        xzzy = (EditText) findViewById(R.id.xzzy);
        xlsj = (EditText) findViewById(R.id.xlsj);

        DigitsKeyListener numberlistener = new DigitsKeyListener(false, true);
        age.setKeyListener(numberlistener);
        sbp.setKeyListener(numberlistener);
        weight.setKeyListener(numberlistener);
        high.setKeyListener(numberlistener);
        pr.setKeyListener(numberlistener);
        xzzy.setKeyListener(numberlistener);
        xlsj.setKeyListener(numberlistener);

        radioman = (RadioButton) findViewById(R.id.radio1);
        radiowomen = (RadioButton) findViewById(R.id.radio2);

        radiopredown = (RadioButton) findViewById(R.id.radio31);
        radionopredown = (RadioButton) findViewById(R.id.radio32);
        get_intent_data();
    }

    private void get_intent_data() {
        Intent intent = getIntent();
        title.setText(intent.getStringExtra("title"));
        adaptcrowd.setText(intent.getStringExtra("adapt_crowd"));
        reportcontentone = intent.getStringExtra("report_content_one");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                back.setBackgroundColor(Color.argb(80, 255, 255, 255));
                finish();
            case R.id.submit:
                if (!radioman.isChecked() && !radiowomen.isChecked()
                        || !radiopredown.isChecked() && !radionopredown.isChecked()
                        || age.getText().toString().equals("")
                        || sbp.getText().toString().equals("")
                        || weight.getText().toString().equals("")
                        || high.getText().toString().equals("")
                        || pr.getText().toString().equals("")
                        || xzzy.getText().toString().equals("")
                        || xlsj.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.answer_not_complete),
                            Toast.LENGTH_SHORT).show();
                } else if (Double.parseDouble(age.getText().toString()) < 45
                        || Double.parseDouble(age.getText().toString()) > 95) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.age_beyond_range), Toast.LENGTH_SHORT)
                            .show();
                } else {

                    String post_data = reportcontentone
                            + getresult(radioman.isChecked(),
                            radiopredown.isChecked(),
                            (int) Double.parseDouble(age.getText()
                                    .toString()),
                            (int) Double.parseDouble(sbp.getText()
                                    .toString()), Double.parseDouble(weight
                                    .getText().toString()),
                            Double.parseDouble(high.getText().toString()),
                            Double.parseDouble(pr.getText().toString()),
                            (int) Double.parseDouble(xzzy.getText()
                                    .toString()),
                            (int) Double.parseDouble(xlsj.getText()
                                    .toString()));
                    String remark = getResources().getString(
                            R.string.report_remark)
                            + "\n"
                            + "参考文献:"
                            + "/n"
                            + "Schnabel RB, Sullivan LM, Levy D, Pencina MJ, Massaro JM, D'Agostino RB, Sr., Newton-Cheh C, Yamamoto JF, Magnani JW, Tadros TM, Kannel WB, Wang TJ, Ellinor PT, Wolf PA, Vasan RS, Benjamin EJ. Development of a risk score for atrial fibrillation (Framingham Heart Study): A community-based cohort study. Lancet. 2009;373:739-745.";
                    showResultDialog(post_data, remark);
                }
                break;
        }
    }

    public void showResultDialog(String result, String remark) {
        final Dialog dialog = new Dialog(TenYearFC.this, R.style.dialog);
        dialog.setContentView(R.layout.dialog_evaluate_report);
        TextView TVResult = (TextView) dialog.findViewById(R.id.tv_evaluate_result);
        TextView TVRemark = (TextView) dialog.findViewById(R.id.tv_remark);
        TVRemark.setText(remark);
        TVResult.setText(result);
        Button IKnown = (Button) dialog.findViewById(R.id.btn_i_know);
        IKnown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }

    private String getresult(boolean isman, boolean isusepredown, int age,
                             int sbp, double weight, double high, double pr, int xzzy, int xlsj) {
        String result = "";
        int score = 0;
        if (isman) {
            if (age >= 45 && age <= 84) {
                score += man_age[(age - 45) / 5];
            } else if (age >= 85) {
                score += 8;
            }
        } else {
            if (age >= 45 && age <= 84) {
                score += women_age[(age - 45) / 5];
            } else if (age >= 85) {
                score += 8;
            }
        }

        if (sbp < 160) {
            score += 0;
        } else {
            score += 1;
        }

        if (isusepredown) {
            score += 1;
        }
        if (weight / (high * high) < 30) {
            score += 0;
        } else {
            score += 1;
        }
        if (pr >= 160 && pr <= 199) {
            score += 1;
        } else if (pr >= 200) {
            score += 2;
        }

        if (age >= 45 && age <= 84) {
            score += xzzy_age[(age - 45) / 10];
        } else if (age >= 85) {
            score += 0;
        }

        if (age >= 45 && age <= 74) {
            score += xlsj_age[(age - 45) / 10];
        } else if (age >= 75) {
            score += 0;
        }

        if (score <= 0) {
            result = "小于0%";

        } else if (score >= 1 && score <= 9) {
            result = pro[score - 1] + "%";

        } else {
            result = ">30%";
        }

        return result;
    }
}
