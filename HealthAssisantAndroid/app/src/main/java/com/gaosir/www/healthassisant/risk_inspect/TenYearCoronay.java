package com.gaosir.www.healthassisant.risk_inspect;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gaosir.www.healthassisant.R;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-06-16
 */
public class TenYearCoronay extends Activity implements View.OnClickListener {

    //TODO 代码结构和命名不好，应当重构。

    private TextView title, adaptcrowd;
    private Button submit;
    private ImageView back;
    private EditText age, sbp, hdlc, tchOrldlc;
    private RadioButton man, woman, smokeYes, smokeNo, diabetesYes, diabetesNo,
            tch, ldlc;
    private String report_content_one,average,lowest;
    private int[] manLdlc_probabililities = { 1, 2, 2, 3, 4, 4, 6, 7, 9, 11, 14,
            18, 22, 27, 33, 40, 47, 56 },manTch_probabililities={2,3,3,4,5,7,8,10,13,16,20,25,31,37,45,53}, womanLdlc_probabililities = { 1, 2, 2, 2,
            3, 3, 4, 5, 6, 7, 8, 9, 11, 13, 15, 17, 20, 24, 27, 32 },womanTch_probabililities = {1,2,2,2,3,3,4,4,5,6,7,8,10,11,13,15,18,20,24,27};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tenyear_coronary);
        initview();
    }

    private void initview() {
        Intent intent = getIntent();
        String titletext = intent.getStringExtra("title");
        String adapttext = intent.getStringExtra("adapt_crowd");
        report_content_one = intent.getStringExtra("report_content_one");
        title = (TextView) findViewById(R.id.title);
        title.setText(titletext);
        adaptcrowd = (TextView) findViewById(R.id.adaptcrowd);
        adaptcrowd.setText(adapttext);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        age = (EditText) findViewById(R.id.age);//
        sbp = (EditText) findViewById(R.id.sbp);//
        tchOrldlc = (EditText) findViewById(R.id.tchOrldlc);//
        hdlc = (EditText) findViewById(R.id.hdlc);//
        man = (RadioButton) findViewById(R.id.man);//
        woman = (RadioButton) findViewById(R.id.woman);//
        smokeYes = (RadioButton) findViewById(R.id.smokeYes);//
        smokeNo = (RadioButton) findViewById(R.id.smokeNo);//
        diabetesYes = (RadioButton) findViewById(R.id.diabetesYes);//
        diabetesNo = (RadioButton) findViewById(R.id.diabetesNo);//
        tch = (RadioButton) findViewById(R.id.tch);//
        ldlc = (RadioButton) findViewById(R.id.ldlc);//
        DigitsKeyListener numericOnlyListener = new DigitsKeyListener(false,true);
        age.setKeyListener(numericOnlyListener);
        sbp.setKeyListener(numericOnlyListener);
        tchOrldlc.setKeyListener(numericOnlyListener);
        hdlc.setKeyListener(numericOnlyListener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                back.setBackgroundColor(Color.argb(80, 255, 255, 255));
                finish();
                break;
            case R.id.submit:
                String Result = "";
                if (this.age.getText().toString().equals("")
                        || tchOrldlc.getText().toString().equals("")
                        || sbp.getText().toString().equals("")
                        || hdlc.getText().toString().equals("") || !man.isChecked()
                        && !woman.isChecked() || !smokeYes.isChecked()
                        && !smokeNo.isChecked() || !diabetesYes.isChecked()
                        && !diabetesNo.isChecked()||!tch.isChecked()&&!ldlc.isChecked()) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.answer_not_complete),
                            Toast.LENGTH_SHORT).show();
                }else{
                    int age = Integer.parseInt(this.age.getText().toString());
                    if(age<30||age>74){
                        Toast.makeText(getApplicationContext(), "年龄不符合要求", Toast.LENGTH_LONG).show();
                    }
                    else {

                        int tchOrldlc = Integer.parseInt(this.tchOrldlc.getText().toString());
                        int sbp = Integer.parseInt(this.sbp.getText().toString());
                        int hdlc = Integer.parseInt(this.hdlc.getText().toString());
                        int score = 0;
                        if (man.isChecked()) {
                            average = getManAverage(age);
                            lowest = getManLowest(age);
                            if(tch.isChecked()){
                                score = getManAgeScore(age) + getManSbpScore(sbp)
                                        + getManTchScore(tchOrldlc) + getManHdlcTchScore(hdlc);}
                            else if(ldlc.isChecked()){
                                score = getManAgeScore(age) + getManSbpScore(sbp)+ getManHdlcLdlcScore(hdlc)
                                        + getManLdlcScore(tchOrldlc);
                            }
                            if (smokeYes.isChecked()) {
                                score += 2;
                            }
                            if (diabetesYes.isChecked()) {
                                score += 2;
                            }
                            if(ldlc.isChecked()){
                                Result = getManLdlcResult(score);
                                System.out.println(score);}
                            else if(tch.isChecked()){
                                Result = getManTchResult(score);
                                System.out.println(score);
                            }
                        } else if (woman.isChecked()) {
                            average = getWomanAverage(age);
                            lowest = getWomanLowest(age);
                            if(tch.isChecked()){
                                score = getWomanAgeScore(age) + getWomanSbpScore(sbp)
                                        + getWomanTchScore(tchOrldlc) + getWomanHdlcTchScore(hdlc);
                            }
                            else if(ldlc.isChecked()){
                                score = getWomanAgeScore(age) + getWomanSbpScore(sbp)
                                        + getWomanHdlcLdlcScore(hdlc)
                                        + getWomanLdlcScore(tchOrldlc);
                            }
                            if (smokeYes.isChecked()) {
                                score += 2;
                            }
                            if (diabetesYes.isChecked()) {
                                score += 4;
                            }
                            if(ldlc.isChecked()){
                                Result = getWomanLdlcResult(score);
                                System.out.println(score);}
                            else if(tch.isChecked()){
                                Result = getWomanTchResult(score);
                                System.out.println(score);
                            }
                        }
                        String post_data = report_content_one +average+"，最低为"+lowest+""+ "根据您目前填写的数据，您10年内患冠心病的风险为"+Result;
                        String remark = getResources().getString(
                                R.string.report_remark)
                                + "\n"
                                + "参考文献:"
                                + "\n"
                                + "Wilson, D'Agostino, Levy et al. 'Prediction of Coronary Heart Disease using Risk Factor Categories', Circulation 1998.";
                        showResultDialog(post_data,remark);
                    }}
                break;

            default:
                break;
        }
    }
    public void showResultDialog(String result, String remark) {
        final Dialog dialog = new Dialog(TenYearCoronay.this, R.style.dialog);
        dialog.setContentView(R.layout.dialog_evaluate_report);
        TextView TVResult = (TextView) dialog.findViewById(R.id.tv_evaluate_result);
        TextView TVRemark = (TextView) dialog.findViewById(R.id.tv_remark);
        TVRemark.setText(remark);
        TVResult.setText(result);
        Button IKnown = (Button) dialog.findViewById(R.id.btn_i_know);
        IKnown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }
    private String getManAverage(int age){
        String average="";
        if (age >= 30 && age <= 34) {
            average="3%";
        } else if (age >= 35 && age <= 39) {
            average="5%";
        } else if (age >= 40 && age <= 44) {
            average="7%";
        } else if (age >= 45 && age <= 49) {
            average="11%";
        } else if (age >= 50 && age <= 54) {
            average="14%";
        } else if (age >= 55 && age <= 59) {
            average="16%";
        } else if (age >= 60 && age <= 64) {
            average="21%";
        } else if (age >= 65 && age <= 69) {
            average="25%";
        } else if (age >= 70 && age <= 74) {
            average="30%";
        }
        return average;

    }
    private String getManLowest(int age){
        String average="";
        if (age >= 30 && age <= 34) {
            average="2%";
        } else if (age >= 35 && age <= 39) {
            average="3%";
        } else if (age >= 40 && age <= 44) {
            average="4%";
        } else if (age >= 45 && age <= 49) {
            average="4%";
        } else if (age >= 50 && age <= 54) {
            average="6%";
        } else if (age >= 55 && age <= 59) {
            average="7%";
        } else if (age >= 60 && age <= 64) {
            average="9%";
        } else if (age >= 65 && age <= 69) {
            average="11%";
        } else if (age >= 70 && age <= 74) {
            average="14%";
        }
        return average;

    }
    private String getWomanAverage(int age){
        String average="";
        if (age >= 30 && age <= 34) {
            average="<1%";
        } else if (age >= 35 && age <= 39) {
            average="<1%";
        } else if (age >= 40 && age <= 44) {
            average="2%";
        } else if (age >= 45 && age <= 49) {
            average="5%";
        } else if (age >= 50 && age <= 54) {
            average="8%";
        } else if (age >= 55 && age <= 59) {
            average="12%";
        } else if (age >= 60 && age <= 64) {
            average="12%";
        } else if (age >= 65 && age <= 69) {
            average="13%";
        } else if (age >= 70 && age <= 74) {
            average="14%";
        }
        return average;

    }
    private String getWomanLowest(int age){
        String average="";
        if (age >= 30 && age <= 34) {
            average="<1%";
        } else if (age >= 35 && age <= 39) {
            average="1%";
        } else if (age >= 40 && age <= 44) {
            average="2%";
        } else if (age >= 45 && age <= 49) {
            average="3%";
        } else if (age >= 50 && age <= 54) {
            average="5%";
        } else if (age >= 55 && age <= 59) {
            average="7%";
        } else if (age >= 60 && age <= 64) {
            average="8%";
        } else if (age >= 65 && age <= 69) {
            average="8%";
        } else if (age >= 70 && age <= 74) {
            average="8%";
        }
        return average;

    }
    private String getManLdlcResult(int score) {
        String result = "";
        if (score <= -3) {
            result = "" + manLdlc_probabililities[0] + "%";
        } else if (score >= 14) {
            result = "" + manLdlc_probabililities[17] + "%";
        } else {
            result = "" + manLdlc_probabililities[score + 3] + "%";
        }
        return result;
    }
    private String getManTchResult(int score) {
        String result = "";
        if (score <= -1) {
            result = "" + manTch_probabililities[0] + "%";
        } else if (score >= 14) {
            result = "" + manTch_probabililities[15] + "%";
        } else {
            result = "" + manTch_probabililities[score + 1] + "%";
        }
        return result;
    }

    private String getWomanLdlcResult(int score) {
        String result = "";
        if (score <= -2) {
            result = "" + womanLdlc_probabililities[0] + "%";
        } else if (score >= 17) {
            result = "" + womanLdlc_probabililities[19] + "%";
        } else {
            result = "" + womanLdlc_probabililities[score + 2] + "%";
        }
        return result;
    }
    private String getWomanTchResult(int score) {
        String result = "";
        if (score <= -2) {
            result = "" + womanTch_probabililities[0] + "%";
        } else if (score >= 17) {
            result = "" + womanTch_probabililities[19] + "%";
        } else {
            result = "" + womanTch_probabililities[score + 2] + "%";
        }
        return result;
    }

    private int getManTchScore(int tch) {
        int score = 0;
        if (tch < 160) {
            score = -3;
        } else if (tch >= 160 && tch <= 199) {
            score = 0;
        } else if (tch >= 200 && tch <= 239) {
            score = 1;
        } else if (tch >= 240 && tch <= 279) {
            score = 2;
        } else if (tch >= 280) {
            score = 3;
        }
        return score;
    }

    private int getWomanTchScore(int tch) {
        int score = 0;
        if (tch < 160) {
            score = -2;
        } else if (tch >= 160 && tch <= 199) {
            score = 0;
        } else if (tch >= 200 && tch <= 239) {
            score = 1;
        } else if (tch >= 240 && tch <= 279) {
            score = 1;
        } else if (tch >= 280) {
            score = 3;
        }
        return score;
    }

    private int getManLdlcScore(int ldlc) {
        int score = 0;
        if (ldlc < 100) {
            score = -3;
        } else if (ldlc >= 100 && ldlc <= 129) {
            score = 0;
        } else if (ldlc >= 130 && ldlc <= 159) {
            score = 1;
        } else if (ldlc >= 160 && ldlc <= 190) {
            score = 2;
        } else if (ldlc >= 190) {
            score = 3;
        }
        return score;
    }

    private int getWomanLdlcScore(int ldlc) {
        int score = 0;
        if (ldlc < 100) {
            score = -2;
        } else if (ldlc >= 100 && ldlc <= 129) {
            score = 0;
        } else if (ldlc >= 130 && ldlc <= 159) {
            score = 0;
        } else if (ldlc >= 160 && ldlc <= 190) {
            score = 2;
        } else if (ldlc >= 190) {
            score = 2;
        }
        return score;
    }

    private int getManHdlcTchScore(int hdlc) {
        int score = 0;
        if (hdlc < 35) {
            score = 2;
        } else if (hdlc >= 35 && hdlc <= 44) {
            score = 1;
        } else if (hdlc >= 45 && hdlc <= 49) {
            score = 0;
        } else if (hdlc >= 50 && hdlc <= 59) {
            score = 0;
        } else if (hdlc >= 60) {
            score = -2;
        }
        return score;
    }
    private int getManHdlcLdlcScore(int hdlc) {
        int score = 0;
        if (hdlc < 35) {
            score = 2;
        } else if (hdlc >= 35 && hdlc <= 44) {
            score = 1;
        } else if (hdlc >= 45 && hdlc <= 49) {
            score = 0;
        } else if (hdlc >= 50 && hdlc <= 59) {
            score = 0;
        } else if (hdlc >= 60) {
            score = -1;
        }
        return score;
    }

    private int getWomanHdlcTchScore(int hdlc) {
        int score = 0;
        if (hdlc < 35) {
            score = 5;
        } else if (hdlc >= 35 && hdlc <= 44) {
            score = 2;
        } else if (hdlc >= 45 && hdlc <= 49) {
            score = 1;
        } else if (hdlc >= 50 && hdlc <= 59) {
            score = 0;
        } else if (hdlc >= 60) {
            score = -3;
        }
        return score;
    }
    private int getWomanHdlcLdlcScore(int hdlc) {
        int score = 0;
        if (hdlc < 35) {
            score = 5;
        } else if (hdlc >= 35 && hdlc <= 44) {
            score = 2;
        } else if (hdlc >= 45 && hdlc <= 49) {
            score = 1;
        } else if (hdlc >= 50 && hdlc <= 59) {
            score = 0;
        } else if (hdlc >= 60) {
            score = -2;
        }
        return score;
    }

    private int getManSbpScore(int sbp) {
        int score = 0;
        if (sbp < 80) {
            score = 0;
        } else if (sbp >= 80 && sbp <= 84) {
            score = 0;
        } else if (sbp >= 85 && sbp <= 89) {
            score = 1;
        } else if (sbp >= 90 && sbp <= 99) {
            score = 2;
        } else if (sbp >= 100) {
            score = 3;
        }
        return score;
    }

    private int getWomanSbpScore(int sbp) {
        int score = 0;
        if (sbp < 80) {
            score = -3;
        } else if (sbp >= 80 && sbp <= 84) {
            score = 0;
        } else if (sbp >= 85 && sbp <= 89) {
            score = 0;
        } else if (sbp >= 90 && sbp <= 99) {
            score = 2;
        } else if (sbp >= 100) {
            score = 3;
        }
        return score;
    }

    private int getManAgeScore(int age) {
        int score = 0;
        if (age >= 30 && age <= 34) {
            score = -1;
        } else if (age >= 35 && age <= 39) {
            score = 0;
        } else if (age >= 40 && age <= 44) {
            score = 1;
        } else if (age >= 45 && age <= 49) {
            score = 2;
        } else if (age >= 50 && age <= 54) {
            score = 3;
        } else if (age >= 55 && age <= 59) {
            score = 4;
        } else if (age >= 60 && age <= 64) {
            score = 5;
        } else if (age >= 65 && age <= 69) {
            score = 6;
        } else if (age >= 70 && age <= 74) {
            score = 7;
        }
        return score;
    }

    private int getWomanAgeScore(int age) {
        int score = 0;
        if (age >= 30 && age <= 34) {
            score = -9;
        } else if (age >= 35 && age <= 39) {
            score = -4;
        } else if (age >= 40 && age <= 44) {
            score = 0;
        } else if (age >= 45 && age <= 49) {
            score = 3;
        } else if (age >= 50 && age <= 54) {
            score = 6;
        } else if (age >= 55 && age <= 59) {
            score = 7;
        } else if (age >= 60) {
            score = 8;
        }
        return score;
    }
}
