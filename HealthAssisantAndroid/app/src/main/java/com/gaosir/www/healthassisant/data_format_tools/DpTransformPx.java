package com.gaosir.www.healthassisant.data_format_tools;

import android.content.Context;

public class DpTransformPx {
	
	public static int dp2px(Context context, float dp) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int)(dp*scale+0.5f);
	}
	
	public static int px2dp(Context context, float px) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (px/scale +0.5f);
	}

}
