package com.gaosir.www.healthassisant;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TabHost;

import com.gaosir.www.healthassisant.risk_inspect.DiseaseRisk;
import com.gaosir.www.healthassisant.secretary.Secretary_modify;
import com.gaosir.www.healthassisant.signs_data.SignsDataTab;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-5-15
 */
public class TabHostOld extends TabActivity {
    private String mHome;
    private String mSecretary;
    private String mSelfExamination;
    private String mNavigation;
    private TabHost tabhost;
    private Resources res;
    private Button ibHome;
    private Button ibSecretary;
    Drawable homepressed, homenormal, parkpressed, parknormal, etcpressed,
            etcnormal, mypressed, mynormal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tab_host);

        findViewById(R.id.btn_home).setOnClickListener(tab_bottom_listener);
        findViewById(R.id.btn_secretary).setOnClickListener(tab_bottom_listener);
        findViewById(R.id.btn_self_examination).setOnClickListener(tab_bottom_listener);
        findViewById(R.id.btn_self_navigation).setOnClickListener(tab_bottom_listener);

        ibHome = (Button) findViewById(R.id.btn_home);
        ibSecretary = (Button) findViewById(R.id.btn_secretary);

        tabhost = getTabHost();
        res = getResources();

        mHome = res.getString(R.string.signs_data);
        mSecretary = res.getString(R.string.secretary);
        mSelfExamination = res.getString(R.string.self_examination_evaluate);
//        mNavigation = res.getString(R.string.self_navigation);
        mNavigation = res.getString(R.string.more);

        // home
        TabHost.TabSpec tab_home = tabhost.newTabSpec(mHome).setIndicator(mHome);
        tab_home.setContent(new Intent(TabHostOld.this, SignsDataTab.class));
        tabhost.addTab(tab_home);

        // secretary
        TabHost.TabSpec tab_secretary = tabhost.newTabSpec(mSecretary).setIndicator(mSecretary);
        tab_secretary.setContent(new Intent(TabHostOld.this, Secretary_modify.class));
        tabhost.addTab(tab_secretary);

        // self check and examination
        TabHost.TabSpec tab_self_examination = tabhost.newTabSpec(mSelfExamination).setIndicator(mSelfExamination);
        tab_self_examination.setContent(new Intent(TabHostOld.this, DiseaseRisk.class));
        tabhost.addTab(tab_self_examination);

        // navigation tab
        TabHost.TabSpec tab_navigation = tabhost.newTabSpec(mNavigation).setIndicator(mNavigation);
//        tab_navigation.setContent(new Intent(TabHostOld.this, SelfExamination.class));
        tab_navigation.setContent(new Intent(TabHostOld.this, MoreTest.class));
        tabhost.addTab(tab_navigation);

        Intent intent = getIntent();
        String current_tag = null;

        current_tag = intent.getStringExtra("tag");
        if (current_tag == null)
            current_tag = mSecretary;
        tabhost.setCurrentTabByTag(current_tag);
        //TODO 切换tab的图标没有对应的常亮
        setCurrentImage(current_tag);

    }

    private void setCurrentImage(String tag) {

        if (tag.equals(mHome)) {
            setImage(R.id.btn_home);
            setTextColor(R.id.btn_home);
        } else if (tag.equals(mSecretary)) {
            setImage(R.id.btn_secretary);
            setTextColor(R.id.btn_secretary);
        } else if (tag.equals(mSelfExamination)) {
            setImage(R.id.btn_self_examination);
            setTextColor(R.id.btn_self_examination);
        } else if (tag.equals(mNavigation)) {
            setImage(R.id.btn_self_navigation);
        } else {
            Log.e("Gao","Tab init error");
        }
    }

    private void setTextColor(int rId) {
    }

    private void setImage(int rId) {
    }

    View.OnClickListener tab_bottom_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_home:
                    Log.i("Gao","home");
                    tabhost.setCurrentTabByTag(mHome);
                    setImage(R.id.btn_home);
                    setTextColor(R.id.btn_home);
                    ibHome.setSelected(true);
                    ibSecretary.setSelected(false);
                    break;
                case R.id.btn_secretary:
                    Log.i("Gao","secretary");
                    tabhost.setCurrentTabByTag(mSecretary);
                    setImage(R.id.btn_secretary);
                    setTextColor(R.id.btn_secretary);
                    ibHome.setSelected(false);
                    ibSecretary.setSelected(true);
                    break;
                case R.id.btn_self_examination:
                    tabhost.setCurrentTabByTag(mSelfExamination);
                    setImage(R.id.btn_self_examination);
                    setTextColor(R.id.btn_self_examination);
                    break;
                case R.id.btn_self_navigation:
                    tabhost.setCurrentTabByTag(mNavigation);
                    break;
            }
        }
    };


}
