package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-6-10
 */
public class ItemData {
    public String user;
    public String itemTag;
    public String item;
    public String time;
    public String date;
    public String value;

    public ItemData(){

    }
    public ItemData(String user,String item,String itemTag,String time,String date,String value){
        this.user = user;
        this.item = item;
        this.value = value;
        this.time = time;
        this.date = date;
        this.itemTag = itemTag;

    }
}
