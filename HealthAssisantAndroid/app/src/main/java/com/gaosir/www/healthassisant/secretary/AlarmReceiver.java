package com.gaosir.www.healthassisant.secretary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
	// private String alarmTitle =null;
	// private String alarmText = null;

	@Override
	public void onReceive(Context context, Intent intent) {

		Intent intent2 = new Intent(context, AlarmDialogSet.class);
		intent2.putExtra("alarmTitle", intent.getStringExtra("alarmTitle"));
		intent2.putExtra("alarmText", intent.getStringExtra("alarmText"));
		intent2.putExtra("end_day", intent.getStringExtra("end_day"));
		intent2.putExtra("alarmID", intent.getIntExtra("alarmID", 0));
		intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent2);

	}

}
