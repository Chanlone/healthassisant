package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlarmDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Alarm.db";
    private static final int DATABASE_VERSION = 1;
    public static final String TB_MEDICAL_REMINDS = "medical_remind";
    public static final String TB_REFERRAL_REMINDS = "referral_remind";
    public static final String TB_ITEM = "heart_blood";
    public static final String TB_EVENT_REMINDS = "event_remind";

    public AlarmDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_MEDICAL_REMINDS + "("
                + "_id INTEGER NOT NULL," + "user_name varchar,"
                + "medical_name varchar," + "dosage varchar,"
                + "start_date varchar," + "end_date varchar,"
                + "remind_morning varchar," + "remind_noon varchar,"
                + "remind_evening varchar," + "repeat_type varchar,"
                + "advance varchar," + "alarmID_1 varchar,"
                + "alarmID_2 varchar," + "alarmID_3 varchar,"
                + "remarks varchar" + ")");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_REFERRAL_REMINDS + "("
                + "_id INTEGER NOT NULL," + "user_name varchar,"
                + "referral_title varchar," + "start_date varchar,"
                + "remind_time varchar," + "remind_place varchar,"
                + "remarks varchar," + "alarmID varchar" + ")");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_EVENT_REMINDS + "("
                + "_id INTEGER NOT NULL," + "user_name varchar NOT NULL,"
                + "event_title varchar," + "start_date varchar,"
                + "remind_time varchar," + "remind_place varchar,"
                + "publish_state varchar," + "notice_to_patient varchar,"
                + "remind_state varchar," + "remind_type varchar,"
                + "remarks varchar," + "alarmID varchar" + ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_ITEM + "("
                + "user_name varchar,"
                + "item varchar," + "value varchar,"
                + "date varchar," + "time varchar,"
                + "item_tag varchar" + ")");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // db.execSQL("ALTER TABLE person ADD COLUMN other STRING");

    }

}
