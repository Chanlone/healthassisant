package com.gaosir.www.healthassisant;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.gaosir.www.healthassisant.intelligent_guild.SelfExamination;
import com.gaosir.www.healthassisant.risk_inspect.DiseaseRisk;


/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-5-28
 */
public class SelfCheckExamination extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.self_check_examination);
        initView();
    }

    private void initView() {
        alertDialog();
    }

    public void alertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.self_examination));
        builder.setItems(R.array.examination_type, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                switch (i){
                    case 0:
                        intent.setClass(SelfCheckExamination.this, DiseaseRisk.class);
                        break;
                    case 1:
                        intent.setClass(SelfCheckExamination.this, SelfExamination.class);
                        break;
                    default:
                        intent.setClass(SelfCheckExamination.this,DiseaseRisk.class);
                }
                startActivity(intent);

            }
        });
        builder.create().show();
    }

    @Override
    public void onClick(View view) {

    }
}
