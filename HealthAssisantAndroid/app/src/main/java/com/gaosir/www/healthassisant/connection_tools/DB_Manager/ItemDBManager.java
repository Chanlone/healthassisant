package com.gaosir.www.healthassisant.connection_tools.DB_Manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * @author Chanlone Gogh
 * @version 1.0.0 2015-6-10
 */
public class ItemDBManager {
    private AlarmDBHelper helper;
    private static SQLiteDatabase db;

    public ItemDBManager(Context context) {
        helper = new AlarmDBHelper(context);
        db = helper.getWritableDatabase();
    }

    public void add(Context context, ItemData itemData) {
        db.beginTransaction();
        try {
            db.execSQL("INSERT INTO " + AlarmDBHelper.TB_ITEM + " VALUES(?,?,?,?,?,?)", new Object[]{
                    itemData.user, itemData.item,
                    itemData.value, itemData.date,
                    itemData.time, itemData.itemTag
            });
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void updateData(Context context, ItemData itemData, String _idString) {
        String whereClause = "_id = ?";
        String[] whereArgs = {_idString};

        ContentValues contentValues = new ContentValues();
        contentValues.put("user_name", itemData.user);
        contentValues.put("item", itemData.item);
        contentValues.put("value", itemData.value);
        contentValues.put("date", itemData.date);
        contentValues.put("time", itemData.time);
        contentValues.put("item_tag", itemData.itemTag);

        db.update(AlarmDBHelper.TB_ITEM, contentValues, whereClause, whereArgs);
    }

    public ItemData addToService(String user, String item) {
        Cursor cursor = queryTheCursor(user, item);
        cursor.moveToLast();

        ItemData itemData = new ItemData();
        itemData.user = cursor.getString(cursor.getColumnIndex("user"));
        itemData.item = cursor.getString(cursor.getColumnIndex("item"));
        itemData.value = cursor.getString(cursor.getColumnIndex("value"));
        itemData.date = cursor.getString(cursor.getColumnIndex("date"));
        itemData.time = cursor.getString(cursor.getColumnIndex("time"));
        itemData.itemTag = cursor.getString(cursor.getColumnIndex("itemTag"));

        cursor.close();
        return itemData;

    }

    public ArrayList<ItemData> query(String user, String item) {
        ArrayList<ItemData> itemDatas = new ArrayList<>();
        Cursor cursor = queryTheCursor(user, item);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                ItemData itemData = new ItemData();
                itemData.user = cursor.getString(cursor.getColumnIndex("user_name"));
                itemData.item = cursor.getString(cursor.getColumnIndex("item"));
                itemData.value = cursor.getString(cursor.getColumnIndex("value"));
                itemData.date = cursor.getString(cursor.getColumnIndex("date"));
                itemData.time = cursor.getString(cursor.getColumnIndex("time"));
                itemData.itemTag = cursor.getString(cursor.getColumnIndex("item_tag"));
                itemDatas.add(itemData);
            }
        }
        cursor.close();
        return itemDatas;
    }


    private static Cursor queryTheCursor(String user, String item) {
        Cursor c = db.rawQuery("SELECT * FROM " + AlarmDBHelper.TB_ITEM + " WHERE user_name = ? And item = ?", new String[]{user, item});
        return c;
    }
}


