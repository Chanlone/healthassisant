package com.gaosir.www.healthassisant.secretary;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gaosir.www.healthassisant.R;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ReferralDBManager;
import com.gaosir.www.healthassisant.connection_tools.DB_Manager.ReferralRemind;

import java.util.ArrayList;
import java.util.Calendar;

public class AddReferralAlarm extends Activity {
	public static final int ALARMID_REFERRAL = 120000000;
	private Calendar mCalendar = Calendar.getInstance();
	private Calendar calendarAlarm = Calendar.getInstance();

	private ReferralRemind referralRemind = null;

	private String alarmTitleString = null;

	private String _idString = null;
	private String tDate = "";

	private int alarmID = 0;
	private String transferType = null;
	private int displayYear = 0;
	private int displayMonth = 0;
	private int displayDay = 0;
	private int displayHour = 0;
	private int displayMinute = 0;

	private ReferralDBManager mReferralDBManager = null;
	private AlarmManager alarmManager = null;
	private EditText referralTitle = null;
	private EditText referralPlace = null;
	private TextView referralDate = null;
	private TextView referralTime = null;
	private EditText remarks = null;

	private Boolean addState = false;
	private Boolean addTstate = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.patient_add_referral);
		initView();
	}

	private void initView() {
		final ImageView back = (ImageView) findViewById(R.id.back);
		TextView addComplete = (TextView) findViewById(R.id.add_referral_complete);

		referralTitle = (EditText) findViewById(R.id.referral_title);
		referralPlace = (EditText) findViewById(R.id.referral_place);
		referralDate = (TextView) findViewById(R.id.referral_date);
		referralTime = (TextView) findViewById(R.id.referral_time);
		remarks = (EditText) findViewById(R.id.referral_remark);

		mReferralDBManager = new ReferralDBManager(getApplicationContext());
		referralRemind = new ReferralRemind();
		referralRemind.user_name = "user";
		// referralRemind.repeat_times = "0";

		Intent it = getIntent();
		transferType = it.getStringExtra("type");

		mCalendar.setTimeInMillis(System.currentTimeMillis());
		calendarAlarm.setTimeInMillis(System.currentTimeMillis());

		alarmTitleString = getString(R.string.subsequent_alarm);

		if (transferType.equals("add")) {
			displayMonth = displayMonth + 1;
			displayYear = mCalendar.get(Calendar.YEAR);
			displayMonth = mCalendar.get(Calendar.MONTH);
			displayDay = mCalendar.get(Calendar.DAY_OF_MONTH);
			displayHour = mCalendar.get(Calendar.HOUR_OF_DAY);
			displayMinute = mCalendar.get(Calendar.MINUTE);

			displayMonth = displayMonth + 1;

			String tTimeString = "";

			if (displayMinute < 10) {
				tTimeString = displayHour + ":0" + displayMinute;
			} else {
				tTimeString = displayHour + ":" + displayMinute;
			}
			referralDate.setText(displayYear + "-" + displayMonth + "-"
					+ displayDay);
			referralTime.setText(tTimeString);
			referralRemind.setRemind_time(tTimeString);
			referralRemind.start_date = displayYear + "-" + displayMonth + "-"
					+ displayDay;

			displayMonth = displayMonth - 1;

		}

		if (transferType.equals("edit")) {

			Log.i("Gao", "edit condition");
			alarmID = it.getIntExtra("alarmID", 0);
			_idString = it.getStringExtra("_id");
			Log.i("id", _idString);
			referralTitle.setText(it.getStringExtra("referral_title"));
			referralPlace.setText(it.getStringExtra("remind_place"));
			remarks.setText(it.getStringExtra("remarks"));

			referralTime.setText(it.getStringExtra("remind_time"));
			referralDate.setText(it.getStringExtra("start_date"));
			String[] dateStrings = it.getStringExtra("start_date").split("-");
			displayYear = Integer.valueOf(dateStrings[0]);
			displayMonth = Integer.valueOf(dateStrings[1]) - 1;
			displayDay = Integer.valueOf(dateStrings[2]);

			setAlarmDate(displayYear, displayMonth, displayDay);
			String[] timeStrings = it.getStringExtra("remind_time").split(":");
			displayHour = Integer.valueOf(timeStrings[0]);
			displayMinute = Integer.valueOf(timeStrings[1]);
			setAlarmTime(displayHour, displayMinute);
			referralRemind.setRemind_time(it.getStringExtra("remind_time"));
			addTstate = true;

		}

		OnClickListener ls = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.back:
					finish();
					break;
				case R.id.add_referral_complete:

					if (referralTitle.getText().toString().equals("")
							|| referralTitle.getText().toString() == null) {
						addState = false;
					} else {
						addState = true;
					}

					addState = addState && addTstate;

					Log.i("Gao", "" + addState);
					
					if (addState) {

						referralRemind.referral_title = referralTitle.getText()
								.toString().trim();
						Log.i("Gao", "referral_title"
								+ referralRemind.referral_title);
						referralRemind.remind_place = referralPlace.getText()
								.toString().trim();
						referralRemind.start_date = referralDate.getText()
								.toString().trim();
						referralRemind.remarks = remarks.getText().toString()
								.trim();
						referralRemind
								.set_id(mReferralDBManager.getMaxID() + 1);

						if (transferType.equals("edit")) {

							AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
							alarmManager.cancel(PendingIntent.getBroadcast(
									getApplicationContext(), alarmID,
									new Intent(getApplicationContext(),
											AlarmReceiver.class), 0));
							if (calendarAlarm.getTimeInMillis() > System
									.currentTimeMillis()) {
								addToAlarm();
								System.out.println(calendarAlarm
										.getTimeInMillis());
								System.out.println(System.currentTimeMillis());
								mReferralDBManager.updateData(
										AddReferralAlarm.this, referralRemind,
										_idString);
								finish();
							} else {
								System.out.println(calendarAlarm
										.getTimeInMillis());
								System.out.println(System.currentTimeMillis());
								Toast toast = Toast.makeText(
										getApplicationContext(),
										getString(R.string.after_current_time), Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER_HORIZONTAL, 0,
										0);
								toast.show();
							}

						} else {
							if (calendarAlarm.getTimeInMillis() > System
									.currentTimeMillis()) {
								System.out.println(calendarAlarm
										.getTimeInMillis());
								System.out.println(System.currentTimeMillis());
								addToAlarm();
								addToDB(referralRemind);
								finish();
							} else {
								System.out.println(calendarAlarm
										.getTimeInMillis());
								System.out.println(System.currentTimeMillis());

								Toast toast = Toast.makeText(
										getApplicationContext(),
										getString(R.string.after_current_time), Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER_HORIZONTAL, 0,
										0);
								toast.show();

							}

							// new Thread() {
							// public void run() {
							// addToService();
							// }
							// }.start();
						}

					} else {
						Toast toast = Toast.makeText(AddReferralAlarm.this,
								R.string.event_reminder_complete,
								Toast.LENGTH_LONG);
						toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
						toast.show();
					}

					break;

				case R.id.referral_date:

					DatePickerDialog startDay = new DatePickerDialog(
							AddReferralAlarm.this, startDatePickListener,
							displayYear, displayMonth, displayDay);
					startDay.show();
					referralRemind.start_date = tDate;
					addTstate = true;

					break;

				case R.id.referral_time:
					new TimePickerDialog(v.getContext(),
							new TimePickerDialog.OnTimeSetListener() {

								@Override
								public void onTimeSet(TimePicker view,
										int hourOfDay, int minute) {
									String tHourString;
									String tMinuteString;
									addTstate = true;

									displayHour = hourOfDay;
									displayMinute = minute;

									setAlarmTime(hourOfDay, minute);
									if (minute < 10) {
										tMinuteString = "0" + minute;
									} else {
										tMinuteString = "" + minute;
									}
									if (hourOfDay < 10) {
										tHourString = "0" + hourOfDay;
									} else {
										tHourString = "" + hourOfDay;
									}
									referralTime.setText(tHourString + ":"
											+ tMinuteString);
									referralRemind.remind_time = ""
											+ tHourString + ":" + tMinuteString;
								}
							}, displayHour, displayMinute, true).show();
					break;

				}
			}

		};

		back.setOnClickListener(ls);
		addComplete.setOnClickListener(ls);
		referralTitle.setOnClickListener(ls);
		referralPlace.setOnClickListener(ls);
		referralDate.setOnClickListener(ls);
		referralTime.setOnClickListener(ls);

	}


	private DatePickerDialog.OnDateSetListener startDatePickListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {

			displayYear = year;
			displayMonth = monthOfYear;
			displayDay = dayOfMonth;

			setAlarmDate(year, monthOfYear, dayOfMonth);
			tDate = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
			referralDate.setText(tDate);
			referralRemind.start_date = tDate;
		}

	};

	private void addToDB(ReferralRemind referralRemind) {

		mReferralDBManager.add(this, referralRemind);
		ArrayList<ReferralRemind> testList = new ArrayList<ReferralRemind>();
		testList = mReferralDBManager.query();
		System.out.println(testList);
	}


	private void setAlarmTime(int hourOfDay, int minute) {

		calendarAlarm.set(Calendar.HOUR_OF_DAY, hourOfDay);
		calendarAlarm.set(Calendar.MINUTE, minute);
		calendarAlarm.set(Calendar.SECOND, 0);
		calendarAlarm.set(Calendar.MILLISECOND, 0);

	}

	private void setAlarmDate(int year, int month, int day) {
		calendarAlarm.set(Calendar.YEAR, year);
		calendarAlarm.set(Calendar.MONTH, month);
		calendarAlarm.set(Calendar.DAY_OF_MONTH, day);

	}

	private void addToAlarm() {

		// Maybe there has a integer overflow.
		int alarmID = (int) (calendarAlarm.getTimeInMillis() / 1000 / 60 + ALARMID_REFERRAL);
		Log.i("Gao", "" + alarmID);

		Log.i("Gao", "addToAlarm start");
		String referral_names = referralRemind.referral_title + " "
				+ mReferralDBManager.alarmIDStrings(this, ""+alarmID);
		Log.i("Gao", referral_names);

		Intent intent = new Intent(AddReferralAlarm.this, AlarmReceiver.class);

		intent.putExtra("alarmTitle", alarmTitleString);
		intent.putExtra("end_day", "0");
		intent.putExtra("alarmID", alarmID);
		intent.putExtra("alarmText", getString(R.string.referral_alarm_text)
				+ " " + referral_names + " "
				+ getString(R.string.alarm_text_tail));

		referralRemind.alarmID = "" + alarmID;

		PendingIntent pIntent = PendingIntent.getBroadcast(
				getApplicationContext(), alarmID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		System.out.println(calendarAlarm.getTimeInMillis());
		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		alarmManager.set(AlarmManager.RTC_WAKEUP, calendarAlarm.getTimeInMillis(), pIntent);

		Log.i("Alarm", "setAlarm complete");
	}

}
